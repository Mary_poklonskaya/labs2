//  Lab 2. Ex 5.
//  Created by Maria Poklonskaya on 05.09.2018.

/*  Написать программу, которая вычисляет дату следующего дня. Например:
 Введите цифрами сегодняшнюю дату (число месяц год) –> 30 4 2016
 Вывести 01.05.2016 */

#include <iostream>
#include <iomanip>

using namespace std;

void output(int day, int month, int year) {
    cout << setw(2) << setfill('0') << day << "." << setw(2) << setfill('0') << month << "." << setw(4) << setfill('0') << year << "\n";
}
void newMonth(int day, int month, int year) { // переход на новый месяц
    day = 1;
    if (month < 12) {
        month = month + 1;
    }
    else {
        month = 1;
        year++;
    }
    cout << "Дата следующего дня: ";
    output(day, month, year);
    
}
void newDay(int day, int month, int year) { 
    day++;
    cout << "Дата следующего дня: ";
    output(day, month, year);
}
void wrongDate() {
    cout << "Такого дня не существует" << "\n";
}
void date(int day, int month, int year, int lastDay) {
    if (day < lastDay) {
        newDay(day, month, year);
    }
    else {
        if (day == lastDay) {
            newMonth(day, month, year);
        }
        else {
            wrongDate();
        }
    }
}
int main() {
    setlocale(LC_ALL, "Russian");
    
    int day;
    int month;
    int year;
    int lastDay;
    cout << "Введите сегодняшнюю дату :\n";
    cin >> day >> month >> year;
    cout << "Введенная дата: ";
    output(day, month, year);
    bool leapYear = (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
    if (month == 2) {
        if (leapYear == true) {
            lastDay = 29;
        }
        else {
            lastDay = 28;
        }
    }
    else {
        if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
            lastDay = 30;
        }
        else {
            if ((month <= 12) && (month > 0)) {
                lastDay = 31;
            }
            else {
                wrongDate();
            }
        }
    }
    if ((year >= 0) && (day > 0) && (month > 0)) {
        date(day, month, year, lastDay);
    }
    else {
        wrongDate();
    }
    system("pause");
}
