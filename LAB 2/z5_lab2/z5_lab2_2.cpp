
�������� ���������, ������� ��������� ���� ���������� ���. ��������:
������� ������� ����������� ���� (����� ����� ���) �> 30 4 2016
������� 30.04.2016

Test:
day		month	year		tomorrow
29		2		1000		����� ���� �� ����������
29		2		2000		01.03.2000
28		2		2000		29.03.2000
28		2		2018		01.03.2018
31		4		2018		����� ���� �� ����������
30		4		2018		01.05.2018
31		5		2018		01.06.2018
32		5		2018		����� ���� �� ����������
31		12		2018		01.01.2019
0		1		1			����� ���� �� ����������
1		0		1			����� ���� �� ����������
1		1		0			02.01.0000
-1		1		1			����� ���� �� ����������
1		-1		1			����� ���� �� ����������
1		1		-1			����� ���� �� ����������
*/

#include <iostream>
#include <iomanip>

using namespace std;
//���������� ����� ����
void output(int day, int month, int year) {
	cout << setw(2) << setfill('0') << day << "." << setw(2) << setfill('0') << month << "." << setw(4) << setfill('0') << year << "\n";
}
//������� �� ����� �����
void newMonth(int day, int month, int year) {
	day = 1;
	if (month < 12) {
		month = month + 1;
	}
	else {
		month = 1;
		year++;
	}
	cout << "���� ���������� ���: ";
	output(day, month, year);

}
//������� �� ����� ����
void newDay(int day, int month, int year) {
	day++;
	cout << "���� ���������� ���: ";
	output(day, month, year);
}
//�������������� ����
void wrongDate() {
	cout << "����� ���� �� ����������" << "\n";
}
//����������� ���� ���������� ���
void date(int day, int month, int year, int lastDay) {
	if (day < lastDay) {
		newDay(day, month, year);
	}
	else {
		if (day == lastDay) {
			newMonth(day, month, year);
		}
		else {
			wrongDate();
		}
	}
}
int main() {
	setlocale(LC_ALL, "Russian");

	int day;
	int month;
	int year;
	int lastDay;//����� ������ ���������� ���� � ������
				//���� ����
	cout << "������� ����������� ���� (����� Enter) :\n";
	cin >> day >> month >> year;
	cout << "��������� ����: ";
	output(day, month, year);
	//����������� ����������� ����
	bool leapYear = (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0));
	//���������� ���������� ���� � ������
	if (month == 2) {
		if (leapYear == true) {
			lastDay = 29;
		}
		else {
			lastDay = 28;
		}
	}
	else {
		if ((month == 4) || (month == 6) || (month == 9) || (month == 11)) {
			lastDay = 30;
		}
		else {
			if ((month <= 12) && (month > 0)) {
				lastDay = 31;
			}
			else {
				wrongDate();
			}
		}
	}
	if ((year >= 0) && (day > 0) && (month > 0)) {
		date(day, month, year, lastDay);
	}
	else {
		wrongDate();
	}
	system("pause");
}
