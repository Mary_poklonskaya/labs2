//  Lab 7. Ex 3.
//  Created by Maria Poklonskaya on 21.10.2018.
/*  Дана строка. Посчитать в ней частоты всех двухбуквенных сочетаний без учета регистра и без учета символов, не являющихся буквами. */
/* Test casing:
 1.  Введите текст:
    helllo
    Сочетание he встречатся 1 раз.
    Сочетание el встречатся 1 раз.
    Сочетание ll встречатся 2 раз.
    Сочетание lo встречатся 1 раз.
    Количество двухбуквенных сочетаний: 5
 2.  Введите текст:
     warning: this program uses gets(), which is unsafe.
     what a wonderful day today
     Сочетание wh встречатся 1 раз.
     Сочетание ha встречатся 1 раз.
     Сочетание at встречатся 1 раз.
     Сочетание wo встречатся 1 раз.
     Сочетание on встречатся 1 раз.
     Сочетание nd встречатся 1 раз.
     Сочетание de встречатся 1 раз.
     Сочетание er встречатся 1 раз.
     Сочетание rf встречатся 1 раз.
     Сочетание fu встречатся 1 раз.
     Сочетание ul встречатся 1 раз.
     Сочетание da встречатся 2 раз.
     Сочетание ay встречатся 2 раз.
     Сочетание to встречатся 1 раз.
     Сочетание od встречатся 1 раз.
     Количество двухбуквенных сочетаний: 17
 3. Введите текст:
 warning: this program uses gets(), which is unsafe.
 aaa aaa. aaa4 a aaaa aaaaaaaa
 Сочетание aa встречатся 16 раз.
 Количество двухбуквенных сочетаний: 16
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include "../../../../libs/array.cpp"

using namespace std;



int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите текст: " << endl;
    char* str = new char [1000];
    char* temp = new char [3];
    char arrAl [100][2];
    gets(str);
    int counter = 0;
    int sizeS = strlen(str);
    for (int i = 0; i < sizeS; ++i){
        if (isalpha(str[i])){
            if (isalpha(str[i+1])){
                arrAl[counter][0] = str[i];
                arrAl[counter][1] = str[i+1];
                counter++;
            }
        }
    }
    for (int i = 0; i < counter; ++i){
        temp = arrAl[i];
        int tempNum = 1;
        for (int j = i+1; j < counter; ++j){
            if (temp[0] == arrAl[j][0] && temp[1] == arrAl[j][1]){
                tempNum++;
                arrAl[j][0] = 0;
            }
        }
        if (isalpha(temp[0])) {
        cout << "Сочетание " << temp[0] << temp[1] << " встречатся " << tempNum << " раз." << endl;
        }
    }
    cout << "Количество двухбуквенных сочетаний: " << counter << endl;
    
    return 0;
}
