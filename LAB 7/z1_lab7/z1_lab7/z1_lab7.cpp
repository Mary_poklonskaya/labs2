//  Lab 7. Ex 1.
//  Created by Maria Poklonskaya on 21.10.2018.
/*  Дана строка символов. Определить количество букв, количество цифр и количество остальных символов, присутствующих в строке. */
/* Test casing:
 1. Введите текст:
    “To live is the rarest thing in the world. Most people exist, that is all.”
     Количество букв в строке(латиница): 56
     Количество цифр в строке: 0
     Количество остальных символов в строке: 23
 2. Введите текст:
     2 * 8 = 16
     Количество букв в строке(латиница): 0
     Количество цифр в строке: 4
     Количество остальных символов в строке: 6
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include "../../../../libs/array.cpp"

using namespace std;


int main() {
    setlocale(LC_ALL, "Russian");
    
    int countLetter = 0;
    int countDigit = 0;
    int countSymb = 0;

    cout << "Введите текст: " << endl;
    char* str = new char [100];
    gets(str); // функция gets() считывает все введённые символы с пробелами до тех пор, пока не будет нажата клавиша Enter

    int size = strlen(str);
    for (int i = 0; i < size; ++i) {
        if (isalpha(str[i])) { // count letters
            ++countLetter;
        }
        else if (isdigit(str[i])) {
                ++countDigit; // count digits
            }
        else
            {
                ++countSymb; // count other symbols
            }
        
    }
    cout << " Количество букв в строке(латиница): " << countLetter << endl;
    cout << " Количество цифр в строке: " << countDigit << endl;
    cout << " Количество остальных символов в строке: " << countSymb << endl;
    
    return 0;
}
