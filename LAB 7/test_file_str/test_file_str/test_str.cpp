#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>

using namespace std;

const int MAX_STRING_LENGTH = 1024;
const char* INPUT_FILE = "input1.txt";
const char* OUTPUT_FILE = "output1.txt";

int enterTwoDimensionalArrayFromFile(int** a, int n, int m, ifstream& fin){
    for (int t = 0; t < n; t++){
        for (int i = 0; i < m; i++){
            fin >> a[t][i];
        }
    }
    return 0;
}

int printTwoDimensionalArray(int **a, int n, int m, ofstream& fout){
    for (int t = 0; t < n; t++){
        for (int i = 0; i < m; i++){
            fout << a[t][i] << " ";
        }
        fout << endl;
    }
    return 0;
}


int main(){
    ifstream fin;
    ofstream fout;

    int n;
    int m;
    int** a;
    fin.open(INPUT_FILE);
    
    fin >> n >> m;
    
    a = new int*[n];
    for (int i = 0; i < n; i++){
        a[i] = new int[m];
    }
    enterTwoDimensionalArrayFromFile(a, n, m, fin);
  
    fout.open(OUTPUT_FILE);
    printTwoDimensionalArray(a, n, m, fout);
    
    fin.close();
    fout.close();
    return 0;
}
