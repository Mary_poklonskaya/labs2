//  Lab 7. Ex 4.
//  Created by Maria Poklonskaya on 21.10.2018.
/*  Дана строка. «Перевернуть» в строке все слова (например: «Жили были дед и баба» - «илиЖ илыб дед и абаб»). Зам. Исходную строку не менять */
/* Test casing:
 1. Введите текст:
 This is a wonderful day
 Перевернутая строка: sihT si a lufrednow yad
 
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include "../../../../libs/array.cpp"

using namespace std;



void reverseWord(char* sLeft, int n)
{
    char* sRight = sLeft + n - 1;
    
    while (sLeft < sRight)
    {
        char t = *sLeft;
        *sLeft = *sRight;
        *sRight = t;
        
        ++sLeft;
        --sRight;
    }
}

char* reverseStr(const char* text, const char* delim)
{
    char* strReverse = new char[256];
    char* s;
    strReverse = strcpy(strReverse, text);
    s = strReverse;
    
    while (*s != '\0')
    {
        int n; // количество букв в слове
        
        if (strchr(delim, *s))
        {
            while (*s != '\0' && strchr(delim, *s)) {
                ++s;
            }
        }
        
        n = 0;
        
        while (s[n] != '\0' && !strchr(delim, s[n])) {
            ++n;
        }
        
        reverseWord(s, n);
        s += n;
    }
    
    return strReverse;
}

int main() {
    setlocale(LC_ALL, "Russian");
    const char* delim = " ,.:;!?\t–";
    cout << "Введите текст: " << endl;
    char* str = new char [1000];
    gets(str);
    
    cout << "Перевернутая строка: " << reverseStr(str, delim) << endl;
    return 0;
}

