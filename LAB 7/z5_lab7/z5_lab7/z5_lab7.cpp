//  Lab 7. Ex 5.
//  Created by Maria Poklonskaya on 21.10.2018.
/*  Дан код программы на С++. Вывести построчно все операции и частоту их использования в программе. */
/* Test casing:
 1.
 
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include "../../../../libs/array.cpp"

using namespace std;

void strToText (char* str, const char* delim){
    
}



int main() {
    setlocale(LC_ALL, "Russian");
   // const char* delim = " ,.:;!?\t";
    cout << "Введите текст: " << endl;
    char* code = new char [1000];
    gets(code);
    
   // cout << "Перевернутая строка: " << reverseStr(str, delim) << endl;
    return 0;
}

/*������� �����        31.10.2018
 5. ��� ��� ��������� �� �++.  ������� ��������� ��� �������� � ������� �� ������������� � ���������.*/
#include <iostream>
#include <fstream>

using namespace std;

const int MAX_TEXT_SIZE = 2000;
const int NUMBER_OF_OPERATORS = 45;

const char** getArrayOfOperators();

int calcSubstrInText(char* text, const char* substr);

void main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    
    if (fin) {
        char text[MAX_TEXT_SIZE];
        const char** operators = getArrayOfOperators();
        
        fin.getline(text, MAX_TEXT_SIZE, EOF);
        
        for (int i = 0; i < NUMBER_OF_OPERATORS; ++i) {
            fout << "\"" << operators[i] << "\"\t" << calcSubstrInText(text, operators[i]) << endl;
        }
        cout << "Ok\n";
        
        delete[] operators;
    }
    else {
        cout << "File not open\n";
    }
    
    system("pause");
}

const char** getArrayOfOperators() {
    const char** operators = new const char*[NUMBER_OF_OPERATORS];
    
    operators[0] = " = ";
    operators[1] = " + ";
    operators[2] = " - ";
    operators[3] = " * ";
    operators[4] = " / ";
    operators[5] = " % ";
    operators[6] = "++";
    operators[7] = "--";
    operators[8] = "==";
    operators[9] = "!=";
    operators[10] = " > ";
    operators[11] = " < ";
    operators[12] = ">=";
    operators[13] = "<=";
    operators[14] = " ! ";
    operators[15] = "&&";
    operators[16] = "||";
    operators[17] = "~";
    operators[18] = " & ";
    operators[19] = " | ";
    operators[20] = " ^ ";
    operators[21] = " << ";
    operators[22] = " >> ";
    operators[23] = "+=";
    operators[24] = "-=";
    operators[25] = "*=";
    operators[26] = "/=";
    operators[27] = "%=";
    operators[28] = "&=";
    operators[29] = "|=";
    operators[30] = "^=";
    operators[31] = "<<=";
    operators[32] = ">>=";
    operators[33] = " & ";
    operators[34] = "->";
    operators[35] = ".";
    operators[36] = ",";
    operators[37] = "?";
    operators[38] = ":";
    operators[39] = "new";
    operators[40] = "delete";
    operators[41] = "delete[]";
    operators[42] = "sizeof";
    operators[43] = "alignof";
    operators[44] = "typeid";
    
    return operators;
}

int calcSubstrInText(char* text, const char* substr) {
    char* pos;
    int count = 0;
    
    pos = strstr(text, substr);
    
    while (pos != 0) {
        ++count;
        pos = strstr(pos + 1, substr);
    }
    
    return count;
}




/*������� �����   26.10.2018
 2.������� �� ������ �������� �����.*/
#include <iostream>
#include <fstream>
#include <locale>

using namespace std;

void eraseWord(char* s, const char* word, const char* delim) {
    if (*s == *word) {
        bool isSub = true;
        int i = 1;
        
        while (*(word + i)) {
            if (*(s + i) != *(word + i)) {
                isSub = false;
                break;
            }
            ++i;
        }
        
        if (isSub) {
            while (*(s + i) != '\0' && strchr(delim, *(s + i))) {
                ++i;
            }
            strcpy(s, s + i);
        }
    }
    ++s;
    
    while (*s) {
        if (strchr(delim, *(s - 1)) && *s == *word) {
            bool isSub = true;
            int i = 1;
            
            while (*(word + i)) {
                if (*(s + i) != *(word + i)) {
                    isSub = false;
                    break;
                }
                ++i;
            }
            
            if (isSub) {
                while (*(s + i) != '\0' && strchr(delim, *(s + i))) {
                    ++i;
                }
                strcpy(s, s + i);
            }
        }
        ++s;
    }
}

void main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    
    setlocale(LC_ALL, "Russian");
    
    char word[50];
    char text[2048];
    char delim[] = " ,.:;!?\t\0";
    size_t size = sizeof(text);
    
    if (fin) {
        fin >> word;
        cout << word << endl;
        fin.ignore();
        fin.getline(text, size);
        cout << text << endl;
        
        eraseWord(text, word, delim);
        cout << text << endl;
    }
    else {
        cout << "���� �� ������" << endl;
    }
    
    system("pause");
} 
