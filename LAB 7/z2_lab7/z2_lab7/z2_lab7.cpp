//  Lab 7. Ex 2.
//  Created by Maria Poklonskaya on 21.10.2018.
/*  Удалить из строки заданное слово */
/* Test casing:
 1. Введите текст:
    You’ve gotta dance like there’s nobody watching
    Введите слово, которое хотите удалить из строки:
    dance
    Строка с удаленным из нее словом: You’ve gotta  like there’s nobody watching
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include "../../../../libs/array.cpp"

using namespace std;

int findWordPos (char* str, char* word) {
    int pos = 0;
    int check = 0;
    int sizeW = strlen (word);
    int sizeS = strlen (str);
    for (int i = 0; i < sizeS; ++i) {
        if (str[i] == word[0]){
            pos = i;
            for (int j = 0; j < sizeW; ++j) {
                if (str[pos] == word[j]){
                    ++pos;
                    ++check;
                }
            }
            if (check == sizeW){
                return i;
            }
        }
        
    }
    
    return -1;
}

char* deleteWordFromStr (char* str, char* word) {
    int sizeW = strlen (word);
    int sizeS = strlen (str);
    int posWord = findWordPos(str, word);
    char* strNew = new char [1000];
    for (int i = 0; i < posWord; ++i) {
        strNew[i] = str[i];
    }
    for (int i = posWord; i < sizeS - sizeW; ++i) {
        strNew[i] = str[i+sizeW];
    }
    
    return strNew;
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите текст: " << endl;
    char* str = new char [1000];
    gets(str);
    cout << "Введите слово, которое хотите удалить из строки: " << endl;
    char* word = new char [100];
    gets(word);
    
    cout << "Строка с удаленным из нее словом: " << deleteWordFromStr(str, word) << endl;
   
    
    return 0;
}
