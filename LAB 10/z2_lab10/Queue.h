#pragma once
#include "Node.h"

template<class T>
class Queue {
private:
	Node<T>* head;
	Node<T>* tail;
public:
    Queue() {
		head = 0;
		tail = 0;
	}

	void push(const T& value) {
		Node<T>* newNode = new Node<T>(value, 0);
		if (head == 0) {
			head = newNode;
		}
		else {
			tail->prev = newNode;
		}
		tail = newNode;
	}
	T& top() {
		return head->value;
	}
	void pop() {
		Node<T>* temp = head;
		head = head->prev;
		delete temp;
	}

	bool isEmpty() {
		return(head == 0);
	}
};
