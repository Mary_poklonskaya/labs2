//
//  main.cpp
//  test_exceptions
//
//  Created by Maria Poklonskaya on 26.11.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#include <iostream>
#include <cmath>

using namespace std;

double calcExpr(double a, double b, double c){
    double d;
    try{
        d = a/b * sqrt(c);
    }
    catch (int x) {
        throw (1);
    }
    return d;
}

int main() {
    double a, b, c, d;
    cin >> a >> b >> c;
    try {
        d = calcExpr(a,b,c);
    }
    catch (int i) {
        cout << i << endl;
    }
    
    return 0;
}
