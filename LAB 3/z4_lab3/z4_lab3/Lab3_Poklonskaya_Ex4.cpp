//Lab 3. Ex 4.
//  Created by Maria Poklonskaya on 17.09.2018.

/*  Для заданного n найти все меньшие его совершенные числа (совершенным называется число, равное сумме всех своих делителей, не равных самому числу).*/

/* Test casing:
 1. Введите натуральное число: 7
    Совершенные числа, которые меньше заданного числа: 6
 2. Введите натуральное число: 500
    Совершенные числа, которые меньше заданного числа: 6 28 496
 3. Введите натуральное число: 8200
    Совершенные числа, которые меньше заданного числа: 6 28 496 8128
 4. Введите натуральное число: 0
    Совершенные числа могут быть только натуральными
 */
#include <iostream>
#include <cmath>

using namespace std;

/*long int sumDel(long int n){ // функциия для определения суммы всех делителей заданного числа
    int sum = 0;
    for (int i = 1; i < n; i++) {
        if (n % i == 0){
            sum = sum + i;
        }
    }
    return sum;
}*/

long int sumDel2(long int n){ // функциия для определения суммы всех делителей заданного числа
    long int sum = 1;
    double k = sqrt(n);
    for (int i = 2; i < k; i++) {
        if (n % i == 0){
            sum = sum + i + n/i;
        }
    }
    if (int(k) * int(k) == n){
        sum = sum + k + n/k;
        cout << '\n' << k << " Проверка на корень" << '\n';
    }
    return sum;
}

int main() {
    setlocale(LC_ALL, "Russian");
    int n;
    cout << "Введите натуральное число: ";
    cin >> n;
    if (n > 0){
        cout << "Совершенные числа, которые меньше заданного числа: ";
        for (int i = 2; i < n; i++){
            if (i == sumDel2(i)){
               cout << i << " ";
            }
        }
    }
    else {
        cout << "Совершенные числа могут быть только натуральными" << endl;
    }
    return 0;
}

