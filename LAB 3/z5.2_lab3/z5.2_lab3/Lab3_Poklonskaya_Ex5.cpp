//Lab 3. Ex 5.
//  Created by Maria Poklonskaya on 17.09.2018.

/* Найти k-ую цифру в ряду цифр, составленных из подряд идущих натуральных чисел, начиная с 1: 12345678910111213… */

/* Test casing:
 1. Введите натуральное число: 100
    5
 2. Введите натуральное число: 10
    1
 3. Введите натуральное число: 7
    7
 4. Введите натуральное число: 4002
    1
 5. Введите натуральное число: 0
    Число не является натуральным
 */
#include <iostream>

using namespace std;

string lastDigit(int n)
{
    char digit[100];
    sprintf(digit,"%d",n);
    return digit;
}

int main()
{
    int n;
    setlocale(LC_ALL, "Russian");
    cout << "Введите натуральное число: ";
    cin >> n;
    string str; // строка, в которую будет записываться ряд чисел
    if (n > 0) {
        for (int i = 0; str.size() <= n; i++) {
            str += lastDigit(i);
        }
        cout << str[n] << endl;
    } else {
        cout << "Число не является натуральным" << endl;
    }
    return 0;
}
