//Lab 3. Ex 1.
//  Created by Maria Poklonskaya on 26.09.2018.

/*  Найти количество различных цифр в записи заданного натурального числа n (разработать алгоритмы с использованием и без использования массивов). */

/* Test casing:
 1. Введите натуральное число: 478832
    Количество различных цифр в записи заданного числа: 5
 2. Введите натуральное число: 48832
    Количество различных цифр в записи заданного числа: 4
 3. Введите натуральное число: 7777
    Количество различных цифр в записи заданного числа: 1
 4. Введите натуральное число: 0
    Число не является натуральным
 */
#include <iostream>
#include <cmath>

using namespace std;

int kolRaslCh(int n, int kol=0) {
    int a = 0;
    int mas[10] = {0};
    while (n >= 1) {
        a = n % 10;
        mas[a] = 1;
        n = n / 10;
    }
    for (int i = 0; i < 10; i++) {
        if (mas[i] == 1) {
            kol++;
        }
    }
    return kol;
}

int kolRaslCh2(int n) {
    int kol = 0;
    int a0 = 0, a1 = 0, a2 = 0, a3 = 0, a4 = 0, a5 = 0, a6 = 0, a7 = 0, a8 = 0, a9 = 0;
    int a;
    while (n >= 1) {
        a = n % 10;
        switch (a) {
            case 0:
                a0++;
                break;
            case 1:
                a1++;
                break;
            case 2:
                a2++;
                break;
            case 3:
                a3++;
                break;
            case 4:
                a4++;
                break;
            case 5:
                a5++;
                break;
            case 6:
                a6++;
                break;
            case 7:
                a7++;
                break;
            case 8:
                a8++;
                break;
            case 9:
                a9++;
                break;
        }
        n = n / 10;
    }
    if (a0 >= 1){
        kol++;
    }
    if (a1 >= 1){
        kol++;
    }
    if (a2 >= 1){
        kol++;
    }
    if (a3 >= 1){
        kol++;
    }
    if (a4 >= 1){
        kol++;
    }
    if (a5 >= 1){
        kol++;
    }
    if (a6 >= 1){
        kol++;
    }
    if (a7 >= 1){
        kol++;
    }
    if (a8 >= 1){
        kol++;
    }
    if (a9 >= 1){
        kol++;
    }
    return kol;
}


int main() {
    setlocale(LC_ALL, "Russian");
    int n;
    int k1; // количество различных цифр
    int k2;
    cout << "Введите натуральное число: ";
    cin >> n;
    if (n > 0){
        k1 = kolRaslCh (n, k1);
        k2 = kolRaslCh2 (n);
        cout << "Количество различных цифр в записи заданного числа: " << k1 << " " << k2 << endl;
    } else {
        cout << "Число не является натуральным" << endl;
    }
    return 0;
}

