 //Lab 3. Ex 5.
//  Created by Maria Poklonskaya on 17.09.2018.

/* Найти k-ую цифру в ряду цифр, составленных из подряд идущих натуральных чисел, начиная с 1: 12345678910111213… */

/* Test casing:
 1. Введите натуральное число: 100
    5
 2. Введите натуральное число: 1
    1
 3. Введите натуральное число: 7
    7
 4. Введите натуральное число: 4002
    1

 */

#include <iostream>
#include <cmath>
#include <cstring>
#include <locale>

using namespace std;


int power(int n, int power){
    int nPower = 1;
    for(int i = 0; i < power; i++){
        nPower *= n;
    }
    return nPower;
}

int xDigitOfNumber(int n, int rightDigit){
    n /= power(10, rightDigit - 1);
    return n % 10;
}

int main() {
    setlocale(LC_ALL, "Russian");
    int k;
    int numberOfDigits = 1;
    int nine = 9;
    int number;
    int digit;
    cout << "Введите натуральное число: ";
    cin >> k;
 
    if (k > 0){
        while(k > 0){
            k -= (nine * numberOfDigits);
            nine *= 10;
            numberOfDigits++;
        }
        nine /= 10;
        numberOfDigits--;
        k = k + nine * numberOfDigits - 1;
        number = power(10, numberOfDigits - 1) + k / numberOfDigits;
        digit = k % numberOfDigits + 1;
        digit = numberOfDigits - digit + 1;
        
        cout << xDigitOfNumber(number, digit) << endl;
    }
    else {
        cout << "Число не натуральное" << endl;
    }
    return 0;
}

