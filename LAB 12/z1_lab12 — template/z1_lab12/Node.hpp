//
//  Node.hpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once
#include <iostream>

using namespace std;
template <class T>
class Node {
public:
    T data;
    int count;
    Node *left, *right;
    Node(T data1, int count1):data(data1), count(count1), left(NULL), right(NULL){};
    void print(){
        cout << data << " - " << count << '\n';
    }
};
