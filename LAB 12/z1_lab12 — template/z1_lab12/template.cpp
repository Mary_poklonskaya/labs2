//
//  main.cpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
/*  Разработать ООП приложение для чтения данных из файла.
    Построения частотного словаря для этих данных,
    и поиска данных в этом словаре.*/

#include <iostream>
#include <fstream>
#include <string>

#include "BinarySearchTree.hpp"

using namespace std;
int main() {
    int n = 1;
    int k = 1;
    while (k != 0){
    cout << "Введите тип данных, с которым будете тестировать программу. 1 - int, 2 - double, 3 - string, 4 - char, 0 - выход из программы: ";
    cin >> k;
    n = 1;
    if (k == 1){
            ifstream fin1("inputInt.txt");
            int m;
            int findWord;
            BinarySearchTree<int> tree;
            while (fin1 >> m){
                tree.add(m);
            }
        while(n != 0){
            cout << "Введите 0 для выхода из программы, введите 1 для вывода частотного словаря, введите 2 для поиска данных в нем: ";
            cin >> n;
            switch (n) {
                case 1:
                    tree.print();
                    break;
                case 2:
                    
                    cout << "Введите слово, которое хотите найти: ";
                    cin >> findWord;
                    tree.findElement(findWord);
            }
        }
    } else if (k == 2){
            ifstream fin2("inputDouble.txt");
            double d;
            double findWord;
            BinarySearchTree<double> tree;
            while (fin2 >> d){
                tree.add(d);
            }
        while(n != 0){
            cout << "Введите 0 для выхода из программы, введите 1 для вывода частотного словаря, введите 2 для поиска данных в нем: ";
            cin >> n;
            switch (n) {
                case 1:
                    tree.print();
                    break;
                case 2:
                    
                    cout << "Введите слово, которое хотите найти: ";
                    cin >> findWord;
                    tree.findElement(findWord);
            }
        }
    } else if (k == 3){
            ifstream fin3("inputString.txt");
            string s;
            string findWord;
            BinarySearchTree<string> tree;
            while (fin3 >> s){
                tree.add(s);
            }
        while(n != 0){
            cout << "Введите 0 для выхода из программы, введите 1 для вывода частотного словаря, введите 2 для поиска данных в нем: ";
            cin >> n;
            switch (n) {
                case 1:
                    tree.print();
                    break;
                case 2:
                    
                    cout << "Введите слово, которое хотите найти: ";
                    cin >> findWord;
                    tree.findElement(findWord);
            }
        }
    } else if (k == 4){
            ifstream fin4("inputChar.txt");
            char c;
            char findWord;
            BinarySearchTree<char> tree;
            while (fin4 >> c){
                tree.add(c);
            }
        while(n != 0){
            cout << "Введите 0 для выхода из программы, введите 1 для вывода частотного словаря, введите 2 для поиска данных в нем: ";
            cin >> n;
            switch (n) {
                case 1:
                    tree.print();
                    break;
                case 2:
                    
                    cout << "Введите слово, которое хотите найти: ";
                    cin >> findWord;
                    tree.findElement(findWord);
            }
        }
    } else if(k != 0 && k != 1 && k != 2 && k != 3 && k != 4) {
        cout << "Неправильно введены запрашиваемые данные! Введите ещё раз: ";
        }
    }
    return 0;
}
