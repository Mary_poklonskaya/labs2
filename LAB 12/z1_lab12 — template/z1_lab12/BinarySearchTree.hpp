//
//  BinarySearchTree.hpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once

#include "Node.hpp"

template <class T>
class BinarySearchTree {
private:
    Node<T>* root;
    void print(Node<T>*);
    Node<T>* add(Node<T>*, T);
    void clear(Node<T>*);
    Node<T>* findElement(Node<T>*, T);
public:
    BinarySearchTree():root(NULL){}
    ~BinarySearchTree(){
        clear(root);
    }
    void print(){
        cout << "Binary Search Tree: " << endl;
        print(root);
    }
    void findElement(T data){
        root = findElement(root, data);
    }
    void add(T data){
        root = add(root, data);
    }
};
template <class T>
void BinarySearchTree<T>::print(Node<T>* node){
    if (node != NULL) {
        print(node->left);
        node->print();
        print(node->right);
    }
}
template <class T>
Node<T>* BinarySearchTree<T>::findElement(Node<T>*node, T data){
    if(node == NULL){
        node = new Node<T>(data, 0);
        node->print();
    } else if (data == node->data) {
        node->print();
    } else if (data < node->data) {
        node->left = findElement(node->left, data);
    } else{
        node->right = findElement(node->right, data);
    }
    return node;
}


template <class T>
void BinarySearchTree<T>::clear(Node<T>* node){
    if (node != NULL) {
        clear(node->left);
        Node<T>* tmp = node->right;
        delete node;
        clear(tmp);
    }
}

template <class T>
Node<T>* BinarySearchTree<T>::add(Node<T>*node, T data){
    if(node == NULL){
        node = new Node<T>(data, 1);
    } else if (data == node->data) {
        (node->count)++;
    } else if (data < node->data) {
        node->left = add(node->left, data);
    } else{
        node->right = add(node->right, data);
    }
    return node;
}

