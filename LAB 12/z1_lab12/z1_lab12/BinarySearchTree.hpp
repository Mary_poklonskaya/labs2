//
//  BinarySearchTree.hpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once

#include "Node.hpp"

class BinarySearchTree {
private:
    Node* root;
    void print(Node*);
    Node* add(Node*, string);
    void clear(Node*);
public:
    BinarySearchTree():root(NULL){}
    ~BinarySearchTree(){
        clear(root);
    }
    void print(){
        cout << "Binary Search Tree: " << endl;
        print(root);
    }
    void add(string data){
        root = add(root, data);
    }
};

