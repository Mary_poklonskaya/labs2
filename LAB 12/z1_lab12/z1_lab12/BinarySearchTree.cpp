//
//  BinarySearchTree.cpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
#
#include "BinarySearchTree.hpp"

void BinarySearchTree::print(Node* node){
    if (node != NULL) {
        print(node->left);
        node->print();
        print(node->right);
    }
}
void BinarySearchTree::clear(Node* node){
    if (node != NULL) {
        clear(node->left);
        Node* tmp = node->right;
        delete[] node;
        clear(tmp);
    }
};
Node* BinarySearchTree::add(Node*node, string data){
    if(node == NULL){
        node = new Node(data, 1);
    } else if (data == node->data) {
        (node->count)++;
    } else if (data < node->data) {
        node->left = add(node->left, data);
    } else{
        node->right = add(node->right, data);
    }
    return node;
}


