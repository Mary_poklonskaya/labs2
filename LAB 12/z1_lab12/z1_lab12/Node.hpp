//
//  Node.hpp
//  z1_lab12
//
//  Created by Maria Poklonskaya on 24.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once
#include <iostream>
#include <string>

using namespace std;
class Node {
public:
    string data;
    int count;
    Node *left, *right;
    Node(string data1, int count1):data(data1), count(count1), left(NULL), right(NULL){};
    void print(){
        cout << data << " - " << count << '\n';
    }
};
