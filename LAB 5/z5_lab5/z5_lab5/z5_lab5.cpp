//  Lab 5. Ex 5.
//  Created by Maria Poklonskaya on 14.10.2018.
/*  Двумерная матрица задает высоты соседствующих клеток местности. Переход возможен в соседние клетки по горизонтали или по вертикали, если высота соседа меньше (как стекает вода).
 1.    Определить все клетки, в которые можно попасть из заданной.
 2.    Определить самую удаленную клетку, в которую можно попасть из заданной.
 3.    Найти путь для подзадачи 2.   */

/* Test casing:
 1. Введите двумерный массив:
 3 1 13 67 44 12 0 7 1
 Введите x начальной клетки: 3
 Необходимо x < n . Введите снова: 1
 Введите y начальной клетки: 1
 -1    1    -1
 -1    0    1
 2    1    2
 Самые удаленные клетки, в которые можно попасть, имеют координаты (x, y):
 (0, 2) (2, 2)
 2. Введите количество строк: 5
 Введите количество столбцов: 5
 Введите двумерный массив:
 23 54 7 87 33 12 35 0 76 4 23 17 9 11 5 28 44 3 13 23 7 76 0 35 1
 Введите x начальной клетки: 3
 Введите y начальной клетки: 0
 -1    -1    1    0    1
 -1    -1    2    1    2
 -1    -1    3    2    3
 -1    -1    4    -1    -1
 -1    -1    5    -1    -1
 Самые удаленные клетки, в которые можно попасть, имеют координаты (x, y):
 (2, 4)
 3. Введите количество строк: 6
 Введите количество столбцов: 6
 Введите двумерный массив:
 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36
 Введите x начальной клетки: 5
 Введите y начальной клетки: 5
 10    9    8    7    6    5
 9    8    7    6    5    4
 8    7    6    5    4    3
 7    6    5    4    3    2
 6    5    4    3    2    1
 5    4    3    2    1    0
 Самые удаленные клетки, в которые можно попасть, имеют координаты (x, y):
 (0, 0)
 4. Введите количество строк: 4
 Введите количество столбцов: 4
 Введите двумерный массив:
 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
 Введите x начальной клетки: 3 1
 Введите y начальной клетки: -1    -1    -1    -1
 -1    -1    -1    0
 -1    -1    -1    -1
 -1    -1    -1    -1
 Самые удаленные клетки, в которые можно попасть, имеют координаты (x, y):
 (3, 1)
 */


#include <iostream>
#include <locale>
#include <queue>



using namespace std;

void inputPositiveNumber(int &n, bool isZero) {
    cin >> n;
    
    if (isZero) {
        while (n < 0) {
            cout << "Число должно быть не отрицательным, введите снова: ";
            cin >> n;
        }
    }
    else {
        while (n <= 0) {
            cout << "Число должно быть положительным, введите снова: ";
            cin >> n;
        }
    }
}

void inputRowsAndColumns(int &strings, int &columns) {
    cout << "Введите количество строк: ";
    cin >> strings;
    while (strings <= 0) {
        cout << "Число не является положительным. Введите снова: ";
        cin >> strings;
    }
    cout << "Введите количество столбцов: ";
    cin >> columns;
    while (columns <= 0) {
        cout << "Число не является положительным. Введите снова: ";
        cin >> columns;
    }
}


int** createMatrix(int strings, int columns) {
    int** a = new int* [strings];
    
    for (int i = 0; i < strings; ++i) {
        a[i] = new int[columns];
    }
    
    return a;
}

void inputMatrix(int** a, int m, int n) {
    cout << "Введите двумерный массив:" << endl;
    
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> a[i][j];
        }
    }
}

void outputMatrix(int** a, int m, int n) {
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << a[i][j] << "\t";
        }
        
        cout << endl;
    }
}

void deleteMatrix(int** a, int m, int n) {
    for (int i = 0; i < m; ++i) {
        delete[] a[i];
    }
    
    delete[] a;
}

int main() {
    setlocale(LC_ALL, "Russian");
    
    int m, n;
    int maxStep = 0;
    int startPosForX, startPosForY;
    int** a = 0;
    int** steps = 0;
    queue <int> stepsInX;
    queue <int> stepsInY;
    
    inputRowsAndColumns(m, n);
    a = createMatrix(m, n);
    inputMatrix(a, m, n);
    
    
    cout << "Введите x начальной клетки: ";
    inputPositiveNumber(startPosForX, true);
    while (startPosForX >= n) {
        cout << "Необходимо x < n . Введите снова: ";
        inputPositiveNumber(startPosForX, true);
    }
    cout << "Введите y начальной клетки: ";
    inputPositiveNumber(startPosForY, true);
    while (startPosForY >= m) {
        cout << "Необходимо y < m . Введите снова: ";
        inputPositiveNumber(startPosForY, true);
    }
    
    steps = createMatrix(m, n);
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            steps[i][j] = -1;
        }
    }
    
    stepsInX.push(startPosForX);
    stepsInY.push(startPosForY);
    steps[startPosForY][startPosForX] = 0;
    
    while (!stepsInX.empty()) {
        int x = stepsInX.front();
        int y = stepsInY.front();
        stepsInX.pop();
        stepsInY.pop();
        
        if (x > 0 && steps[y][x - 1] == -1 && (a[y][x - 1] < a[y][x])) {
            steps[y][x - 1] = steps[y][x] + 1;
            stepsInX.push(x - 1);
            stepsInY.push(y);
        }
        if (x + 1 < n && steps[y][x + 1] == -1 && (a[y][x + 1] < a[y][x])) {
            steps[y][x + 1] = steps[y][x] + 1;
            stepsInX.push(x + 1);
            stepsInY.push(y);
        }
        if (y > 0 && steps[y - 1][x] == -1 && (a[y - 1][x] < a[y][x])) {
            steps[y - 1][x] = steps[y][x] + 1;
            stepsInX.push(x);
            stepsInY.push(y - 1);
        }
        if (y + 1 < m  && steps[y + 1][x] == -1 && (a[y + 1][x] < a[y][x])) {
            steps[y + 1][x] = steps[y][x] + 1;
            stepsInX.push(x);
            stepsInY.push(y + 1);
        }
        
        maxStep = steps[y][x];
    }
    
    outputMatrix(steps, m, n);
    
    cout << "Самые удаленные клетки, в которые можно попасть, имеют координаты (x, y): \n";
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            if (steps[i][j] == maxStep) {
                cout << "(" << j << ", " << i << ") ";
            }
        }
    }
    
    deleteMatrix(a, m, n);
    deleteMatrix(steps, m, n);
    
    cout << endl;
    return 0;
}
