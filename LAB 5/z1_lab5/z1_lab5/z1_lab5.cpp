//  Lab 5. Ex 1.
//  Created by Maria Poklonskaya on 14.10.2018.
/*  Дана квадратная матрица n-го порядка, состоящая из целых чисел. Определить является ли она магическим квадратом (Магический квадрат — квадратная таблица n × n, заполненная n2 различными числами таким образом, что сумма чисел в каждой строке, каждом столбце и на обеих диагоналях одинакова). Для решения задачи использовать функцию поиска суммы элементов i-того столбца и строки, массив передается в функцию через указатель. */
/* Test casing:
 1. Введите количество строк в квадратной матрице: 3
    Введите элементы матрицы(9) : 1 2 3 4 5 6 7 8 9
    Не является магическим квадратом
 2. Введите количество строк в квадратной матрице: 4
    Введите элементы матрицы(16) : 5 7 2 9 7 1 3 6 5 9 7 11 2 0 4 8
    Не является магическим квадратом
 3. Введите количество строк в квадратной матрице: 3
    Введите элементы матрицы(9) : 16 213 119 219 116 13 113 19 216
    Является магическим квадратом
 4. Введите количество строк в квадратной матрице: 4
    Введите элементы матрицы(16) : 16 3 2 13 5 10 11 8 9 6 7 12 4 15 14 1
    Является магическим квадратом
 5. Введите количество строк в квадратной матрице: 3
    Введите элементы матрицы(9) : 2 2 2 2 2 2 2 2 2
    Не является магическим квадратом, так как все числа в матрице равны между собой
 */


#include <iostream>
#include <locale>


using namespace std;


int sumString(int** a, int i, int n) {
    int sum = 0;
    for (int j = 0; j < n; j++){
        sum += a[i][j]; // суммы в строках
    }
    return sum;
}

int sumColumn (int** a, int j, int n) {
    int sum = 0;
    for (int i = 0; i < n; i++) {
        sum += a[i][j]; // суммы в столбцах
    }
    return sum;
}

bool isMagical (int** a, int n) {
  
    int magicSum = 0;
    for (int i = 0; i < n; i++) {
        magicSum += a[i][0]; // пример магической суммы для последующих сравнений
    }
    
    for (int i = 0; i < n; i++) {
        int sum = 0;
        sum = sumString(a, i, n);
        if (sum != magicSum){
            return false;
        }
    }

    for (int j = 0; j < n; j++) {
        int sum = 0;
        sum = sumColumn(a, j, n);
        if (sum != magicSum){
            return false;
        }
        
    }
    int sum = 0;
    for (int i = 0; i < n; i++){
        sum += a[i][i]; // сумма в первой диагонали
    }
    if (sum != magicSum){
        return false;
    }
    
    sum = 0;
    for (int i = 0; i < n; i++)
        sum += a[i][n - i - 1]; // сумма во второй диагонали
    if (sum != magicSum){
        return false;
    }
    
    return true;

}


void inputMatrix(int** a, int n) { // ввод двумерного массива
    cout << "Введите элементы матрицы(" << n*n << ") : ";
    for (int i = 0; i != n; i++){
        a[i] = new int[n];        // выделение памяти под числовые значения
        for (int j = 0; j != n; j++){
            cin >> a[i][j];       //  инициализация
        }
    
    }
}

void outputMatrix(int** a, int n){
    for (int i = 0; i != n; i++){
        for (int j = 0; j != n; j++){
            cout.width(3);
            cout << a[i][j];
        }
        cout << endl;
    }
    cout << endl;
}

void deleteMatrix(int** a, int m) {
    for (int i = m-1; i < 0; --i) {
        delete[] a[i];
    }
    
    delete[] a;
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите количество строк в квадратной матрице: " ;
    int n;
    cin >> n;
    int** a = new int*[n];     // объявление и выделение памяти под другие указатели
    inputMatrix(a, n);
    bool isCorrect = true;
    int exEl = a[n-1][n-1];
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (a[i][j] == exEl){
                isCorrect = false;
            } else {
                isCorrect = true;
                break;
            }
        }
    }
    if (isCorrect) {
        if (isMagical(a,n)) {
            cout << "Является магическим квадратом" << endl;
        } else {
            cout << "Не является магическим квадратом" << endl;

        }
    } else {
        cout << "Не является магическим квадратом, так как все числа в матрице равны между собой" << endl;
    }
    
   //outputMatrix(a, n);
    deleteMatrix(a, n);
    
    return 0;
}
