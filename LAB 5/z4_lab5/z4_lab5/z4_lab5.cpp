//  Lab 5. Ex 3.
//  Created by Maria Poklonskaya on 14.10.2018.
/*  Написать программу, которая содержит функцию, принимающую в качестве аргумента, указатели на два массива (А и В) и размеры массивов. Функция проверяет, является ли массив В подмножеством/построкой (2 способами, см. зам.) массива А и возвращает указатель на начало найденного фрагмента, если элемента нет, возвращает 0.
 Зам. 1) Множество B называется подмножеством множества A, если все элементы, принадлежащие B, также принадлежат A.
 2) Подстрока – это непустая связная часть строки.
 */
/* Test casing:
1. Введите массив из 5 элементов: 1 2 3 4 5
 Введите массив из 2 элементов: 2 4
 В является подмножеством А
 В не является подстрокой A
2. Введите массив из 4 элементов: 1 3 4 2
 Введите массив из 3 элементов: 4 1 1
 В не является подмножеством A
 В не является подстрокой A
3. Введите количество элементов в массивax: 5 8
 Чтобы множество В являлось подмножеством/пожстрокой множества А нужно, чтобы количество элементов в нем не превышало количество элементов в А
4. Введите массив из 7 элементов: 1 2 3 5 6 7 7
 Введите массив из 3 элементов: 3 6 1
 В является подмножеством А
 В не является подстрокой A
 */


#include <iostream>
#include <locale>


using namespace std;


void copyArray (int* arrayFrom,int *arrayTo, int n) {
    for (int i = 0; i < n; ++i) {
        arrayTo [i] = arrayFrom [i];
    }
}

void insertionSort (int* a, int n) {
    for (int i = 1; i < n; ++i) {
        int temp = a[i];
        int j = i-1;
        while (temp < a[j] && j >= 0) {
            a[j+1] = a[j];
            --j;
        }
        a[j+1] = temp;
    }
}

int* findPosEl (int* begin, int* end, int key){
    int* pos = 0;
    for (int* i = begin; i < end; ++i){
        if (*i == key){
            pos = i;
        }
    }
    return pos;
}


int isSubset(int* a,int* b, int n1, int n2){
    int* aCopy = new int [n1];
    copyArray(a, aCopy, n1);
    int* bCopy = new int [n2];
    copyArray(b, bCopy, n2);
    insertionSort(aCopy, n1);
    insertionSort(bCopy, n2);
    int* last = aCopy;
    bool isSubset = true;
   // while (!isSubset){
    for (int i = 0; i < n2; ++i){
        last = findPosEl(last, aCopy + n1, bCopy[i]);
        if (last != 0){
            ++last;        } else {
            isSubset = false;
            break;
        }
        
    }
    return isSubset;
    }




int isSubstring (int* a,int* b, int n1, int n2) {
    int* last = a;
    bool isSubstring = false;
    while (!isSubstring){
        int* current = findPosEl(last, a + n1, b[0]);
        if (current != 0){
            isSubstring = true;
            for (int i = 1; i < n2; ++i){
                if (b[i] != *(current + i)){
                    isSubstring = false;
                    break;
                }
            }
            last = current + 1;
        } else {
            break;
        }
    }
    return isSubstring;
}
    

void inputArray(int n, int* a){
    cout << "Введите массив из "<< n <<" элементов: ";
    for (int i = 0; i < n; ++i){
        cin >> a[i];
    }
}


void outputArray(int n, int* a){
    cout << "Отсортированный массив: ";
    for (int i = 0; i < n; ++i){
        cout << a[i] << " ";
    }
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите количество элементов в массивax: " ;
    int n1;
    cin >> n1;
    int* A = new int [n1];
    int n2;
    cin >> n2;
    int* B = new int [n2];
    
    if (n1 >= n2) {
        inputArray(n1, A);
        inputArray(n2, B);
    
        if (isSubset(A, B, n1, n2)) {
            cout << "В является подмножеством А" << endl;
        } else {
            cout << "В не является подмножеством A" << endl;
        }
        if (isSubstring(A, B, n1, n2)) {
            cout << "В является подстрокой А" << endl;
        } else {
            cout << "В не является подстрокой A" << endl;
        }
    } else {
        cout << "Чтобы множество В являлось подмножеством/пожстрокой множества А нужно, чтобы количество элементов в нем не превышало количество элементов в А" << endl;
    }
    delete[] A;
    delete[] B;
    
    return 0;
}
