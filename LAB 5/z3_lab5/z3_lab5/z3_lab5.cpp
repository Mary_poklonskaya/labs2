//  Lab 5. Ex 3.
//  Created by Maria Poklonskaya on 14.10.2018.
/*  Написать функцию, которая производит двоичный поиск места размещения нового элемента в упорядоченном массиве и возвращает указатель на место включения нового элемента. С помощью данной функции реализовать сортировку вставками.  */
/* Test casing:
 1. Введите количество элементов в массиве: 4
    Введите массив из 4 элементов: 4 5 2 3
    Отсортированный массив: 2 3 4 5
 2. Введите количество элементов в массиве: 5
    Введите массив из 5 элементов: 5 4 3 2 1
    Отсортированный массив: 1 2 3 4 5
 3. Введите количество элементов в массиве: 5
    Введите массив из 5 элементов: 7 2 5 9 0
    Отсортированный массив: 0 2 5 7 9
 */


#include <iostream>
#include <locale>


using namespace std;

void swapElements(int& a, int& b){
    int temp = a;
    a = b;
    b = temp;
}

int binarySearch(int n, int* a, int key){
    int lower = 0;
    int upper = n;
    int midPos;
    while (lower < upper) {
        midPos = lower + (upper-lower)/2;
        if (key < a[midPos]){
            upper = midPos;
        } else if (key > a[midPos]) {
            lower = midPos + 1 ;
        } else {
            return midPos;
        }
    }
    return lower;
}

int insertionBinarySort (int n, int* arr) {
    int pos;
    for (int i = 0; i < n; ++i) {
        int temp = arr[i];
        pos = binarySearch(i,arr,temp);
        for (int j = i ; j >= pos + 1 ; --j) {
             arr[j] = arr[j - 1];
        }
        arr[pos] = temp;
    }
    return *arr;
}

void inputArray(int n, int* a){
    cout << "Введите массив из "<< n <<" элементов: ";
    for (int i = 0; i < n; ++i){
        cin >> a[i];
        }
    }


void outputArray(int n, int* a){
    cout << "Отсортированный массив: ";
    for (int i = 0; i < n; ++i){
        cout << a[i] << " ";
    }
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите количество элементов в массиве: " ;
    int n;
    cin >> n;
    int* a = new int [n];
    
    inputArray(n, a);
    insertionBinarySort (n, a);
    outputArray(n, a);
    
    delete[] a;
    return 0;
}
