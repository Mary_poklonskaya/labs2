//  Lab 5. Ex 2.
//  Created by Maria Poklonskaya on 14.10.2018.
/*  Заполнить квадратную матрицу (n x n) натуральными четными числами 2,4,6,8,... по сходящейся к центру линии ( по спирали) */
/* Test casing:
 1. Введите количество строк в квадратной матрице: 3
 2    4    6
 16   18    8
 14   12   10
 2. Введите количество строк в квадратной матрице: 4
 2    4    6    8
 24   26   28   10
 22   32   30   12
 20   18   16   140
 3. Введите количество строк в квадратной матрице: 7
 2    4    6    8   10   12   14
 48   50   52   54   56   58   16
 46   80   82   84   86   60   18
 44   78   96   98   88   62   20
 42   76   94   92   90   64   22
 40   74   72   70   68   66   24
 38   36   34   32   30   28   26

 */


#include <iostream>
#include <locale>


using namespace std;


void inputMatrix(int** a, int n) { // ввод двумерного массива
    cout << "Введите элементы матрицы(" << n*n << ") : ";
    for (int i = 0; i != n; i++){
        a[i] = new int[n];     // выделение памяти под числовые значения
        for (int j = 0; j != n; j++){
            cin >> a[i][j];         //  инициализация
        }
    }
}

void inputMatrixWithEvenNum(int** a, int n) { // заполнение двумерного массива по сходящейся к центру линии
    for (int i = 0; i != n; i++){
        a[i] = new int[n];     // выделение памяти под числовые значения
        for (int j = 0; j != n; j++){
            a[i][j] = 0;         //  инициализация нулями
        }
    }
    int ch = 2; // значение первого элемента
    int lastEl = n-1; // верхняя граница (для незаполненных ячеек)
    int firstEl = 0; // нижняя граница (для незаполненных ячеек)
    int i = 0;
    int j = 0;

    while (firstEl <= lastEl ) {
        
        while (j < lastEl){ // заполнение слева направо
            a[i][j] = ch;
            ch += 2;
            ++j;
        }
        while (i < lastEl){ // заполнение сверху вниз
            a[i][j] = ch;
            ch += 2;
            ++i;
        }
        while (j > firstEl){ //заполнение справа налево
            a[i][j] = ch;
            ch += 2;
            --j;
        }
        firstEl++; // переход на следующий уровень спираль (ближе к центру)
        lastEl--;
        while (i > firstEl){ // заполнение снизу вверх
            a[i][j] = ch;
            ch += 2;
            --i;
        }
    }
    a[i][j] = ch;
}


void outputMatrix(int** a, int n){
    for (int i = 0; i != n; i++){
        for (int j = 0; j != n; j++){
            cout.width(5);
            cout << a[i][j];
        }
        cout << endl;
    }
    cout << endl;
}
void deleteMatrix(int** a, int m) {
    for (int i = m-1; i < 0; --i) {
        delete[] a[i];
    }
    
    delete[] a;
}

int main() {
    setlocale(LC_ALL, "Russian");
    cout << "Введите количество строк в квадратной матрице: " ;
    int n;
    cin >> n;
    if (n > 0){
        int** a = new int*[n];     // объявление и выделение памяти под другие указатели
       
        inputMatrixWithEvenNum(a, n);
        
        outputMatrix(a, n);
        deleteMatrix(a, n);
    }
    
    return 0;
}
