// Лабораторная работа 9. Задание 3

//  Created by Maria Poklonskaya on 11.11.2018.
/*  Дана строка, которая представляет собой исходное математическое выражение, содержащее целые числа, операции +,  *, -, /  и скобки любой степени вложенности. Перевести его в обратную польскую запись и вычислить значение записанного выражения.*/
/*
 Test casing:
 1. Введите математическое выражение (в виде строки символов):
    3+4*2/(1-5)
    342*15-/+
 2. Введите математическое выражение (в виде строки символов):
    4-(3*3+6)/3+9
    Обратная польская запись выражения:
    433*6+3/-9+
    Значение выражения:
    8
 3. Введите математическое выражение (в виде строки символов):
    (9-3*2-(6*3)+4/2)
    Обратная польская запись выражения:
    932*-63*-42/+
    Значение выражения:
    -13
 4. Введите математическое выражение (в виде строки символов):
    4-8*(3+5]
    Скобки расставлены неправильно. Перепроверьте введенную запись.
 */

#include <iostream>
#include <exception>
#include <cstdlib>
//#include "./../../Stack.h"

using namespace std;

template <typename T>
struct Stack
{
    Stack(){
        top_s = NULL;
        size_s = 0;
    }
    struct Node{
        Node(T el){
            data = el;
        }
        Node(T el, Node* node){
            data = el;
            next = node;
        }
        Node* next;
        T data;
    };
    
    void push(const T);
    void pop();
    T& top();
    int size();
    bool empty();
    
private:
    Node* top_s;
    int size_s;
};

template <typename T>
void Stack<T>::push(const T el)
{
    try {
        if(top_s != NULL){
            Node* temp = new Node(el, top_s);
            top_s = temp;
        } else {
            top_s = new Node(el);
        }
        size_s++;
    } catch (bad_alloc) {
        cout << "bad_alloc" << endl;
    }
}

template <typename T>
void Stack<T>::pop()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        Node* temp = top_s;
        top_s = top_s -> next;
        delete temp;
        size_s--;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
}

template <typename T>
T & Stack<T>::top()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        return top_s -> data;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
    return top_s -> data; //
}

template <typename T>
int Stack<T>::size(){
    return size_s;
}

template <typename T>
bool Stack<T>::empty(){
    return size_s == 0 ? true : false;
}

bool brackets(const char* txt){
    Stack <char> stk;
    char c;
    for (int i = 0; (c = txt[i]); ++i){
        switch  (c){
            case '(' : case '{' : case '[' :
                stk.push(c);
                break;
            case ')':
                if (stk.top() != '('){
                    return false;
                }
                stk.pop();
                break;
            case '}':
                if (stk.top() != '{'){
                    return false;
                }
                stk.pop();
                break;
            case ']':
                if (stk.top() != '['){
                    return false;
                }
                stk.pop();
                break;
        }
    }
    return stk.empty();
}

char* polishReverse(const char* txt){
    Stack <char> stkOper;
    Stack <char> tempe;
    char* revText = new char [20];
    char c;
    int j = 0;
    for (int i = 0; (c = txt[i]); ++i){
        switch (c) {
            case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
                revText[j] = c;
                ++j;
                break;
            case '+':
                while (!stkOper.empty()){
                    if (stkOper.top() == '+' || stkOper.top() == '-' || stkOper.top() == '/' || stkOper.top() == '*') {
                        revText[j] = stkOper.top();
                        stkOper.pop();
                        ++j;
                    } else {
                        break;
                    }
                }
                stkOper.push(c);
                break;
            case '-':
                while (!stkOper.empty()){
                    if (stkOper.top() == '+'|| stkOper.top() == '/' || stkOper.top() == '-' || stkOper.top() == '*'){
                        revText[j] = stkOper.top();
                        stkOper.pop();
                        ++j;
                    } else {
                        break;
                    }
                }
                stkOper.push(c);
                break;
            case '*':
                if (!stkOper.empty()){
                    if (stkOper.top() == '/'){
                        revText[j] = stkOper.top();
                        stkOper.pop();
                        ++j;
                    }
                }
                stkOper.push(c);
                break;
            case '/':
                if (!stkOper.empty()){
                    if (stkOper.top() == '*'){
                        revText[j] = stkOper.top();
                        stkOper.pop();
                        ++j;
                    }
                }
                stkOper.push(c);
                break;
            case '(':
                stkOper.push(c);
                break;
            case ')':
                while (stkOper.top() != '('){
                    revText[j] = stkOper.top();
                    stkOper.pop();
                    ++j;
                }
                stkOper.pop();
                break;
        }
    }
    
    while(!stkOper.empty()){
        tempe.push(stkOper.top());
        stkOper.pop();
    }
    while(!tempe.empty()){
        revText[j] = tempe.top();
        tempe.pop();
        ++j;
    }
    return revText;
}
double strCalc(char* revText){
    Stack <double> stk;
    char c;
    double temp = 0;
    for (int i = 0; (c = revText[i]); ++i){
        switch (c) {
            case '0':
                stk.push(0);
                break;
            case '1':
                stk.push(1);
                break;
            case '3':
                stk.push(3);
                break;
            case '4':
                stk.push(4);
                break;
            case '5':
                stk.push(5);
                break;
            case '6':
                stk.push(6);
                break;
            case '7':
                stk.push(7);
                break;
            case '8':
                stk.push(8);
                break;
            case '2':
                stk.push(2);
                break;
            case '9':
                stk.push(9);
                break;
            case '+':
                temp = stk.top();
                stk.pop();
                temp = temp + stk.top();
                stk.pop();
                stk.push(temp);
                break;
            case '-':
                temp = stk.top();
                stk.pop();
                temp = stk.top()-temp;
                stk.pop();
                stk.push(temp);
                break;
            case '*':
                temp = stk.top();
                stk.pop();
                temp *= stk.top();
                stk.pop();
                stk.push(temp);
                break;
            case '/':
                temp = stk.top();
                stk.pop();
                temp = stk.top()/temp;
                stk.pop();
                stk.push(temp);
                break;
        }
    }
    return stk.top();
}
//3+4*2/(1-5)
int main() {
    char* txt = new char[20];
    cout << "Введите математическое выражение (в виде строки символов): " << endl;
    cin >> txt;
    bool isCorrect = brackets(txt);
    while(!isCorrect){
        cout << "Скобки расставлены неправильно. Перепроверьте введенную запись." << endl;
        cin >> txt;
        isCorrect = brackets(txt);
    }
    cout << "Обратная польская запись выражения: " << endl;
    char* newTxt = polishReverse(txt);
    cout << newTxt << endl;
    cout << "Значение выражения: " << endl;
    cout << strCalc(newTxt) << endl;
    return 0;
}
