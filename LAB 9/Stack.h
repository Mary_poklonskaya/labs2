#pragma once
#include "Node.h"

template <class T>
class Stack
{
private:
    Node* top_s;
    int size_s;
public:
    Stack(){
        top_s = NULL;
        size_s = 0;
    }
    
    void push(const T el)
    {
            if(top_s != NULL){
                Node* temp = new Node(el, top_s);
                top_s = temp;
            } else {
                top_s = new Node(el);
            }
            size_s++;
    }
    void pop()
    {
        try {
            if (top_s == NULL){
                throw out_of_range("Error: out of range");
            }
            Node* temp = top_s;
            top_s = top_s -> next;
            delete temp;
            size_s--;
        }
        catch (exception & ex) {
            cout << ex.what() << endl;
        }
    }
    T& top()
    {
        try {
            if (top_s == NULL){
                throw out_of_range("Error: out of range");
            }
            return top_s -> data;
        }
        catch (exception & ex) {
            cout << ex.what() << endl;
        }
        return top_s -> data; //
    }
    int size(){
        return size_s;
    }
    bool isEmpty(){
        return size_s == 0 ? true : false;
    }
    ~Stack() {
        
    }
};
