// Лабораторная работа 9. Задание 2
//  Created by Maria Poklonskaya on 11.11.2018.
/*  Пусть дана без ошибок формула, имеющая следующий синтаксис:
    <формула>::=<цифра> | max(<формула>, < формула >) | min (<формула>,< формула >)
    <цифра>::=0  1  2  3  4  5  6  7  8   9
    Например, 8 или max(4, min(3, 5)) или  min(min(3, 5), max(2, max(3, 4))).
    Вычислить значение формулы, используя структуру данных стек.
 */
/*
 Test casing:
 1.
 */

#include <iostream>
#include <exception>

using namespace std;

template <typename T>
struct Stack
{
    Stack(){
        top_s = NULL;
        size_s = 0;
    }
    struct Node{
        Node(T el){
            data = el;
        }
        Node(T el, Node* node){
            data = el;
            next = node;
        }
        Node* next;
        T data;
    };
    
    void push(const T);
    void pop();
    T& top();
    int size();
    bool empty();
    
private:
    Node* top_s;
    int size_s;
};

template <typename T>
void Stack<T>::push(const T el)
{
    try {
        if(top_s != NULL){
            Node* temp = new Node(el, top_s);
            top_s = temp;
        } else {
            top_s = new Node(el);
        }
        size_s++;
    } catch (bad_alloc) {
        cout << "bad_alloc" << endl;
    }
}

template <typename T>
void Stack<T>::pop()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        Node* temp = top_s;
        top_s = top_s -> next;
        delete temp;
        size_s--;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
}

template <typename T>
T & Stack<T>::top()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        return top_s -> data;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
    return top_s -> data; //
}

template <typename T>
int Stack<T>::size(){
    return size_s;
}

template <typename T>
bool Stack<T>::empty(){
    return size_s == 0 ? true : false;
}


int main() {
    
    return 0;
}
