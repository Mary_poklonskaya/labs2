#pragma once

template <class T>

class Node {
public:
    T value;
    Node<T>* prev;
    Node(const T& value, Node<T>* prev){
        this -> value = value;
        this -> prev = prev;
    }
};
