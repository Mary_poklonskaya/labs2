// Лабораторная работа 9. Задание 1

//  Created by Maria Poklonskaya on 11.11.2018.
/*  Дано математическое выражение в виде строки символов. Напишите программу, которая определит, правильно ли расставлены скобки в выражении, если оно состоит из скобок типа: ( ) [ ] { }. (использовать структуру данных стек).*/
/*
Test casing:
 1. Введите математическое выражение в виде строки символов:
    (56+0)=[(34-8*5)-9]
    Скобки расставлены правильно.
 2. Введите математическое выражение в виде строки символов:
    (56+{3=[(34-8*5)-9]
    Скобки расставлены неправильно.
 */

#include <iostream>
#include <exception>

using namespace std;

template <typename T>
struct Stack
{
    Stack(){
        top_s = NULL;
        size_s = 0;
    }
    struct Node{
        Node(T el){
            data = el;
        }
        Node(T el, Node* node){
            data = el;
            next = node;
        }
        Node* next;
        T data;
    };
  
    void push(const T);
    void pop();
    T& top();
    int size();
    bool empty();
    
private:
    Node* top_s;
    int size_s;
};

template <typename T>
void Stack<T>::push(const T el)
{
    try {
        if(top_s != NULL){
            Node* temp = new Node(el, top_s);
            top_s = temp;
        } else {
            top_s = new Node(el);
        }
        size_s++;
    } catch (bad_alloc) {
        cout << "bad_alloc" << endl;
    }
}

template <typename T>
void Stack<T>::pop()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        Node* temp = top_s;
        top_s = top_s -> next;
        delete temp;
        size_s--;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
}

template <typename T>
T & Stack<T>::top()
{
    try {
        if (top_s == NULL){
            throw out_of_range("Error: out of range");
        }
        return top_s -> data;
    }
    catch (exception & ex) {
        cout << ex.what() << endl;
    }
    return top_s -> data; //
}

template <typename T>
int Stack<T>::size(){
    return size_s;
}

template <typename T>
bool Stack<T>::empty(){
    return size_s == 0 ? true : false;
}

bool brackets(const char* txt){
    Stack <char> stk;
    char c;
    for (int i = 0; (c = txt[i]); ++i){
        switch  (c){
            case '(' : case '{' : case '[' :
                stk.push(c);
                break;
            case ')':
                if (stk.top() != '('){
                    return false;
                }
                stk.pop();
                break;
            case '}':
                if (stk.top() != '{'){
                    return false;
                }
                stk.pop();
                break;
            case ']':
                if (stk.top() != '['){
                    return false;
                }
                stk.pop();
                break;
        }
    }
    return stk.empty();
    
}

int main() {
    char* txt = new char[10];
    cout << "Введите математическое выражение в виде строки символов: " << endl;
    cin >> txt;
    bool isCorrect = brackets(txt);
    if (isCorrect){
        cout << "Скобки расставлены правильно." << endl;
    } else {
        cout << "Скобки расставлены неправильно." << endl;
    }
return 0;
}
