//  Lab 4. Ex 5.
//  Created by Maria Poklonskaya on 07.10.2018.
/*  ) Добавить к некоторой последовательности  A=(ai), i=1..n, n<=100, наименьшее  число элементов,  чтобы  получить  арифметическую прогрессию (например, 22 1  36  -> 1  8 15 22 29 36).  */


#include <iostream>
#include <locale>
//#include "../../../libs/array.h"


using namespace std;



void inputArray(int* a, int n) { // ввод массива
    for (int i = 0; i < n; ++i) {
        cin >> *(a + i);
    }
}

void insertionSort(int* a, int n) {
    int temp;
    for (int i = 1; i < n; ++i) {
        temp = a[i];
        int j = i;
        
        while (j > 0 && a[j - 1] > temp) {
            a[j] = a[j - 1];
            --j;
        }
        
        a[j] = temp;
    }
}


int gcd(int a, int b)
{
    return b ? gcd(b, a % b) : a;
}


void ArithmeticProgression(int* a, int n) {
    int gcdNum;
    if (n != 1) {
        insertionSort(a, n);
        
        gcdNum = a[1] - a[0];
        for (int i = 1; i < n - 1; ++i) {
            gcdNum = gcd(gcdNum, a[i + 1] - a[i]);
        }
        
        cout << "Арифметическая последовательность: ";
        for (int curr = a[0]; curr <= a[n - 1]; curr += gcdNum) {
            cout << curr << " ";
        }
    }
    else {
        cout << a[0];
    }
    
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    
    int N = 100;
    int n;
    int a[N];
    
    cout << "Введите натуральное число - количество элементов последовательности: ";
    cin >> n;
    
    cout << "Введите последовательность: ";
    inputArray(a, n);

    ArithmeticProgression(a, n);
    
  
    
    
    return 0;
    
}

