//  Lab 4. Ex 2.
//  Created by Maria Poklonskaya on 07.10.2018.
/* 2. Дан массив целых чисел, из этой последовательности целых чисел выбрать три числа, произведение которых максимально.*/


#include <iostream>
#include <locale>


using namespace std;

void inputArray(int* a, int n) { // ввод массива
    for (int i = 0; i < n; ++i) {
        cin >> *(a + i);
    }
}

void swap(int& a, int& b) {
    int temp = a;
    a = b;
}

void maxMult(int* a, int n) {
    int curr;
    int mult1;
    int mult2;
    
    for (curr = 0; curr < 2; ++curr) {
        int i;
        int min = a[curr];
        int minPos = curr;
        for (i = curr + 1; i < n; ++i) {
            if (a[i] < min) {
                min = a[i];
                minPos = i;
            }
        }
        swap(a[curr], a[minPos]);
    }
    
    for (int i = n - 1; i > n - 4; --i) {
        int j;
        int max = a[i];
        int maxPos = i;
        
        for (j = i - 1; j >= curr; --j) {
            if (a[j] > max) {
                max = a[j];
                maxPos = j;
            }
        }
        swap(a[i], a[maxPos]);
    }
    
    
    mult1 = a[0] * a[1] * a[n - 1];
    mult2 = a[n - 1] * a[n - 2] * a[n - 3];
    
    if (mult1 > mult2) {
        cout << a[0] << " " << a[1] << " " << a[n - 1];
    }
    else if(mult2 == mult1 && n != 3) {
        cout << "Произведение чисел " << a[0] << " " << a[1] << " " << a[n - 1]
        << " равно произведению чисел " << a[n - 3] << " " << a[n - 2] << " " << a[n - 1];
    }
    else {
        cout << a[n - 3] << " " << a[n - 2] << " " << a[n - 1];
    }
    
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    
    int N = 100;
    int n;
    int a[N];
    
    cout << "Введите натуральное число - размер массива: ";
    cin >> n;
    while (n < 3) {
        cout << "Число должно быть не меньше трех, исходя из условия. Введите снова: ";
        cin >> n;
    }
    
    cout << "Введите элементы массива: ";
    inputArray(a, n);

    maxMult(a, n);
    
    return 0;
    
}
