//  Lab 4. Ex 3.
//  Created by Maria Poklonskaya on 07.10.2018.
/*  Дан массив целых чисел. Реализовать функцию для обработки массива целых чисел. Функция находит минимальный элемент массива и сортирует все элементы левее его по убыванию, а правее его – по возрастанию. Функция возвращает индекс минимального элемента.  */
/*Test casing:
 1. Введите натуральное число - размер массива: 5
    Введите элементы массива: -3 8 -2 5 0
    Индекс минимального элемента: 0
    Обработанный массив: -3 -2 0 5 8
 2. Введите натуральное число - размер массива: 6
    Введите элементы массива: 3 2 -5 8 0 9
    Индекс минимального элемента: 2
    Обработанный массив: 3 2 -5 0 8 9
 3. Введите натуральное число - размер массива: 4
    Введите элементы массива: 1 1 1 1
    Индекс минимального элемента: 0
    Обработанный массив: 1 1 1 1
 */

#include <iostream>
#include <locale>


using namespace std;



void inputArray(int* a, int n) { // ввод массива
    for (int i = 0; i < n; ++i) {
        cin >> *(a + i);
    }
}

int findMinPos(int* a, int n) {
    int minPos = 0;
    int min;
    
    min = a[0];
    
    for (int i = 1; i < n; ++i) {
        if (a[i] < min) {
            min = a[i];
            minPos = i;
        }
    }
    
    return minPos;
}

void insertionSort(int* a, int n) {
    int temp;
    for (int i = 1; i < n; ++i) {
        temp = a[i];
        int j = i;
        
        while (j > 0 && a[j - 1] > temp) {
            a[j] = a[j - 1];
            --j;
        }
        
        a[j] = temp;
    }
}

void insertionSortDown(int* a, int n) {
    int temp;
    for (int i = 1; i < n; ++i) {
        temp = a[i];
        int j = i;
        
        while (j > 0 && a[j - 1] < temp) {
            a[j ] = a[j - 1];
            --j;
        }
        
        a[j] = temp;
    }
}

int symmetrySortFromMin(int* a, int n) {
    int minPos = findMinPos(a, n);
    
    insertionSortDown(a, minPos);
    insertionSort(a + minPos + 1, n - minPos - 1);
    
    return minPos;
}

void outputArray(int* a, int n) {
    for (int i = 0; i < n; ++i) {
        cout << a[i] << " ";
    }
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    
    int N = 100;
    int n;
    int a[N];
    
    cout << "Введите натуральное число - размер массива: ";
    cin >> n;
    
    cout << "Введите элементы массива: ";
    inputArray(a, n);
    
    cout << "Индекс минимального элемента: " << symmetrySortFromMin(a, n) << endl;
    cout << "Обработанный массив: ";
    outputArray(a, n);

    
    return 0;
    
}
