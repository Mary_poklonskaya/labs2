//  Lab 4. Ex 4.
//  Created by Maria Poklonskaya on 07.10.2018.
/*  Найти сумму и произведение двух многочленов, заданных массивами своих коэффициентов.  */
/*Test casing:
 1. Введите количество слагаемых первого многочлена: 3
    Введите количество слагаемых второго многочлена: 4
    Введите коэффициенты многочлена(по возрастанию стоящих рядом с ними степеней) размером 3: 1 4 5
    Введите коэффициенты многочлена(по возрастанию стоящих рядом с ними степеней) размером 4: 5 2 1 3
    Коэффициенты суммы многочленов: 6 6 6 3
    Коэффициенты произведения многочленов: 5 22 34 17 17 15
 2. Введите количество слагаемых первого многочлена: 1
    Введите количество слагаемых второго многочлена: 3
    Введите коэффициенты многочлена (по возрастанию стоящих рядом с ними степеней) размером 1: 3
    Введите коэффициенты многочлена (по возрастанию стоящих рядом с ними степеней) размером 3: 4 2 1
    Коэффициенты суммы многочленов: 7 2 1
    Коэффициенты произведения многочленов: 12 6 3
 */

#include <iostream>
#include <locale>


using namespace std;



void inputArray(int* a, int n) { // ввод массива
    for (int i = 0; i < n; ++i) {
        cin >> *(a + i);
    }
}


void sumOfPolynomial(int* a, int* b, int* sum, int sumLength) {
    for (int i = 0; i < sumLength; ++i) {
        sum[i] = a[i] + b[i];
    }
}

void multOfPolynomial(int* a, int n, int* b, int m,  int* mult) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            mult[j + i] += a[i] * b[j];
        }
    }
}

void outputArray(int* a, int n) {
    for (int i = 0; i < n; ++i) {
        cout << a[i] << " ";
    }
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    const int N = 100;
    int n;
    int m;
    int a[N] = {};
    int b[N] = {};
    int sum[N] = {};
    int sumLength;
    int mult[N] = {};
    int multLength;
    
    cout << "Введите количество слагаемых первого многочлена: ";
    cin >> n;
    cout << "Введите количество слагаемых второго многочлена: ";
    cin >> m;
    cout << "Введите коэффициенты многочлена (по возрастанию стоящих рядом с ними степеней) размером " << n << ": ";
    inputArray(a, n);
    cout << "Введите коэффициенты многочлена (по возрастанию стоящих рядом с ними степеней) размером " << m << ": ";
    inputArray(b, m);
    
    sumLength = max(n, m);
    multLength = n + m - 1;
    
    if (sumLength <= N) {
        sumOfPolynomial(a, b, sum, sumLength);
        
        cout << "Коэффициенты суммы многочленов: ";
        outputArray(sum, sumLength);
    } else {
        cout << "Размер массива суммы больше максимального!\n";
    }
    
    if (multLength <= N) {
        multOfPolynomial(a, n, b, m, mult);
        
        cout << "Коэффициенты произведения многочленов: ";
        outputArray(mult, multLength);
    } else {
        cout << "Размер массива произведения больше максимального!\n";
    }
    
    return 0;
    
}
