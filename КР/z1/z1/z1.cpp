//  Контрольная работа. Вариант 1. Задание 1
//  Created by Maria Poklonskaya.
/*  Отсортировать слова текста по возрастанию доли гласных букв (отношение количества гласных букв к общему количеству букв в слове). */
/* Test casing:
 1. Введите текст:
    Binary operators are typically implemented as non-members to maintain symmetry
    Отсортированные слова текста:
    non-members implemented symmetry operators typically Binary as to maintain are
 2. Введите текст:
    Basically, the reason we are using a string is that 'strings are good for dealing with words.'
    Отсортированные слова текста:
    strings string words that with the for using dealing Basically reason we is good are are a
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
#include <cstring>
//#include "../../../../libs/array.cpp"

using namespace std;


void insertionSortWords (int n, double* a, char* str[n]) {
    for (int i = 1; i < n; ++i) {
        double temp = a[i];
        char* wTempe = str[i];
        int j = i-1;
        while (temp < a[j] && j >= 0) {
            a[j+1] = a[j];
            str[j+1] = str[j];
            --j;
        }
        a[j+1] = temp;
        str[j+1] = wTempe;
    }
}


double vowelPropInWord (char* pch) {
    size_t n = strlen(pch);
    const char VOVELS[] = "eyuioaEYUIOA";
    int numOfVowels = 0;
    for (int i = 0; i < n; ++i){
        if (strchr(VOVELS, pch[i])){
            ++numOfVowels;
        }
    }
    return ((double)numOfVowels/(double)n);
}

void sortStrByVowelProportion (char* str, const char* delim){
    char* arrWord[500];
    int size = 10;
    double* arrProp = new double[size];
    int k;
    char* pch;
    pch = strtok(str, " ,.:;!?\t'()"); //Здесь знаки, отделяющие слова
    k = 0;
    while (pch != NULL)
    {
        arrWord[k] = pch;
        arrProp[k] = vowelPropInWord(pch);
        k++;
        pch = strtok(NULL, " ,.:;'!?\t()");
    }
    insertionSortWords(k, arrProp, arrWord);
    cout << "Отсортированные слова текста:" << endl;
    for (int i = 0; i < k; i++)
    {
        cout << arrWord[i] << " ";
    }
    cout << endl;
}

int main() {
    setlocale(LC_ALL, "Russian");
    const char* delim = " ,.:;'!?\t()";
    cout << "Введите текст: " << endl;
    char* str = new char [500];
    gets(str);

    sortStrByVowelProportion(str, delim);

    return 0;
}
