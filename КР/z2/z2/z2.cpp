//  Контрольная работа. Вариант 1. Задание 2
//  Created by Maria Poklonskaya.
/*  Дана матрица размера m x n. "Уплотнить" ее, удаляя из нее строки и столбцы, заполненные нулями.*/
/* Test casing:
 1. Введите количество строк и столбцов в двумерном массиве: 3 4
    Введите двумерный массив:
    1 2 0 3 0 0 0 0 4 5 0 6
    Исходная матрица:
        1    2    0    3
        0    0    0    0
        4    5    0    6
    'Уплотненная' матрица:
        1    2    3
        4    5    6
 2. Введите количество строк и столбцов в двумерном массиве: 4 5
 Введите двумерный массив:
 4 1 0 5 0
 3 2 0 4 0
 0 0 0 0 0
 1 1 0 1 0
 Исходная матрица:
 4    1    0    5    0
 3    2    0    4    0
 0    0    0    0    0
 1    1    0    1    0
 'Уплотненная' матрица:
 4    1    5
 3    2    4
 1    1    1
 */


#include <iostream>
#include <locale>
#include <cmath>
#include <fstream>
#include <ctime>
//#include "../../../../libs/array.cpp"

using namespace std;
int strings, columns = 0;

bool strWithNull(int** a, int i, int n){
    bool isNull = true;
    for (int j = 0; j < n; ++j) {
        if (a[i][j] != 0){
            isNull = false;
            return isNull;
        }
    }
    return isNull;
}

bool columnWithNull(int** a, int j, int m){
    bool isNull = true;
    for (int i = 0; i < m; ++i) {
        if (a[i][j] != 0){
            isNull = false;
            return isNull;
        }
    }
    return isNull;
}



int** createMatrix(int strings, int columns) {
    int** a = new int* [strings];
    
    for (int i = 0; i < strings; ++i) {
        a[i] = new int[columns];
    }
    
    return a;
}

void inputMatrix(int** a, int m, int n) {
    cout << "Введите двумерный массив:" << endl;
    
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cin >> a[i][j];
        }
    }
}

void outputMatrix(int** a, int m, int n) {
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < n; ++j) {
            cout << a[i][j] << "\t";
        }
        
        cout << endl;
    }
}

void deleteMatrix(int** a, int m, int n) {
    for (int i = 0; i < m; ++i) {
        delete[] a[i];
    }
    
    delete[] a;
}

int** deleteColumn (int** a, int j, int n, int m){
    int** newArray = new int*[m];
    for (int i = 0; i < m; i++){
        newArray[i] = new int[n-1];
    }
    for (int i = 0; i < m; ++i){
        for (int k = 0; k < j; ++k){
            newArray[i][k] = a[i][k];
        }
        for (int k = j; k < n-1; ++k){
            newArray[i][k] = a[i][k+1];
        }
    }
   // --n;
    return newArray;
}

int** deleteString (int** a, int fl, int n, int m){
    int** newArray = new int*[m-1];
    for (int i = 0; i < m; i++){
        newArray[i] = new int[n];
    }
    for (int i = 0; i < fl; ++i){
        for (int j = 0; j < n; ++j){
            newArray[i][j] = a[i][j];
        }
    }
    for (int i = fl; i < m-1; ++i){
        for (int j = 0; j < n; ++j){
            newArray[i][j] = a[i+1][j];
        }
    }
   // --m;
    return newArray;
}

int** arrWithDeletedNullParts(int** a, int n, int m){
     for (int i = 0; i < m; ++i){
         if (strWithNull(a, i, n) == true){
             a = deleteString(a, i, n, m);
             --m;
        }
     }
    
    for (int j = 0; j < n; ++j){
        if (columnWithNull(a, j, m) == true){
            a = deleteColumn(a, j, n, m);
            n--;
        }
    }
    ::strings = m;
    ::columns = n;
    
    return a;
}

int main() {
    setlocale(LC_ALL, "Russian");
    int m, n;
    cout << "Введите количество строк и столбцов в двумерном массиве: " ;
    cin >> m >> n;
    int** a = 0;
    
    a = createMatrix(m, n);
    inputMatrix(a, m, n);
     cout << "Исходная матрица: " << endl;
    outputMatrix(a, m, n);
    a = arrWithDeletedNullParts(a, n, m);
    cout << "'Уплотненная' матрица: " << endl;
    outputMatrix(a, strings, columns);
    deleteMatrix(a, m, n);
    return 0;
}
