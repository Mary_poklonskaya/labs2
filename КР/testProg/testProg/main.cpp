#include <iostream>
#include <string.h>

using namespace std;

int main()
{
    int k;
    char a[1000];
    char* arr[100];
    char* str;
    cout << "Введите строку из слов:" << endl;
    gets(a);
    str = strtok(a, " ,.-"); //Здесь знаки, отделяющие слова
    k = 0;
    while (str != NULL)
    {
        arr[k] = str;
        k++;
        str = strtok(NULL, " ,.-");
    }
    cout << "Элементы массива:" << endl;
    for (int i = 0; i < k; i++)
    {
        cout << arr[i] << endl;
    }
    
    double x, y;
    double rezult;
    
    while (1)
    {
        std::cout << "x=";
        std::cin >> x;
        std::cout << "y=";
        std::cin >> y;
        
        rezult=x/y;
        std::cout << "Rezult: " << rezult;
        std::cout << std::endl << std::endl;
    }
    
    system("pause");
    return 0;
}


int* bubbleSortWithDop (int n, double* a) {
    
    int* arrTemp = new int [n];
    for (int i = 0; i < n; ++i){
        arrTemp[i] = i;
    }
    
    for (int k = 0; k < n - 1; ++k) {
        for (int i = 0; i < n - k -1; ++i){
            if (a[i] > a[i+1]) {
                int temp = a[i];
                a[i] = a[i+1];
                a[i+1] = temp;
                
                int temp2 = arrTemp[i];
                arrTemp[i] = arrTemp[i+1];
                arrTemp[i+1] = temp2;
            }
        }
    }
    return arrTemp;
}

char* sortText(int n, double* a, char str[n]){
    int* arrTemp = bubbleSortWithDop(n, a);
    char* finishedText = new char[n];
    for (int i = 0; i < n; ++i){
        int k = arrTemp[i];
        finishedText[i] = str[k];
    }
    
    return finishedText;
}
