//
//  Rational.hpp
//  z1_lab10
//
//  Created by Maria Poklonskaya on 12.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
/* Рациональная (несократимая) дробь представляется парой целых чисел (a,b), где a – числитель, b – знаменатель. Создать класс Rational для работы с рациональными дробями. Реализовать конструкторы: копирования и инициализации, а также методы:
 a.    сложение add, (a,b) + (c,d) = (ad+bc,bd);
 b.    умножение mul, (a,b) * (c,d) = (ac, bd);
 c.    деление div, (a,b)/(c,d) = (ad,bc);
 d.    сравнение equal, greater, less;
 e.    вывод print.
 Зам. Реализовать приватную функцию сокращения дроби reduce, которая обязательно вызывается при выполнении арифметических операций.
 */

#pragma once

int gcd(int a, int b);

class Rational {
private:
    int a, b;
    void reduce(){
        int nod = gcd(a, b);
        a /= nod;
        b /= nod;
    }
public:
    Rational(int = 0, int = 1);
   // Rational(int);
   // Rational();
    Rational(const Rational&);
    void print();
    Rational operator+ (Rational r2);
    Rational operator- (Rational r2);
    Rational operator* (Rational r2);
    Rational operator/ (Rational r2);
    bool operator== (Rational r2);
    bool operator< (Rational r2);
    bool operator<= (Rational r2);
    bool operator> (Rational r2);
    bool operator>= (Rational r2);

};

bool operator!= (Rational r1, Rational r2);
