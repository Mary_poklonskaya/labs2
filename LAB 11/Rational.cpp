//
//  Rational.cpp
//  z1_lab10
//
//  Created by Maria Poklonskaya on 12.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//


#include <iostream>
#include <cmath>
#include "Rational.hpp"
#include "RationalException.hpp"


using namespace std;


Rational::Rational(int a, int b){
    if (b == 0) {
        throw RationalException ("Конструктор: знаменатель равен нулю");
    }
    this->a = a;
    this->b = b;
    reduce();
}

/*Rational::Rational(int a){
    this->a = a;
    b = 1;
}

Rational::Rational(){
    a = 0;
    b = 1;
}*/

Rational::Rational(const Rational& r2){
    a = r2.a;
    b = r2.b;
}

Rational Rational::operator+ (Rational r2){
    return Rational(a * r2.b + r2.a * b, b * r2.b);
}

Rational Rational::operator- (Rational r2){
    return Rational(a * r2.b - r2.a * b, b * r2.b);
}

Rational Rational::operator* (Rational r2){
    return Rational(a * r2.a, b * r2.b);
}

Rational Rational::operator/ (Rational r2){
    if (r2.a == 0){
         throw RationalException ("Деление на нуль");
    }
    return Rational(a * r2.b, b * r2.a);
}

bool Rational::operator== (Rational r2){
    return (a == r2.a && b == r2.b);
}

bool Rational::operator< (Rational r2){
    return (a * r2.b < r2.a * b);
}

bool Rational::operator<= (Rational r2){
    return (a * r2.b <= r2.a * b);
}

bool Rational::operator> (Rational r2){
    return (a * r2.b < r2.a * b);
}

bool Rational::operator>= (Rational r2){
    return (a * r2.b < r2.a * b);
}

void Rational::print(){
    cout << a << " / " << b << endl;
}

int gcd(int a, int b)
{
    return b ? gcd(b, a % b) : a;
}

bool operator!= (Rational r1, Rational r2){
    return !(r1 == r2);
}
