//
//  main.cpp
//  z1_lab11
//
//  Created by Maria Poklonskaya on 16.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
/* 7.    Исходные данных хранить в виде списка операций.
 8.    Данные в исходном файле представлены имеют следующую структуру:
 числитель/знаменатель операция числитель/знаменатель
 При этом, обязательным в дроби является только числитель (в этом случае знаменатель считается равным 1), и операция и вторая дробь могут отсутствовать.
 9.    В исходном файле не все строки могут быть корректными, это не должно приводить к аварийному завершению программы. Вычисления необходимо будет провести только для корректных данных (для строк, содержащих корректные данные).
 10.    При работе приложения вести лог (записывать в файл log/log.txt), записывать информацию о том, когда и какой файл загружен или записан, номерах некорректных строк в исходном файле и т.д.
 Пример исходных данных:
 3/2 + 8/13
 -9
 -1/5
 4/5 * 3/7
 9/23 - 3
 6 / 0
 5/7 / 0/5
 -9/11 * -7/12
 */

#include <iostream>
#include <fstream>

using namespace std;

#include "Rational.hpp"
#include "RationalException.hpp"
#include "Operation.hpp"


int main() {
    try {
        Operation arr[100];
        char buffer[100];
        int n = 0;
        ifstream fin("in.txt");
        
        while (!fin.eof()){
            fin.getline(buffer, 100);
            //cout << buffer << endl;
            try {
                arr[n] = Operation(buffer);
                ++n;
            }
            catch(RationalException exc) {
                cout << "OperationException " << exc.getMessage() << endl;
            }
        }
        cout << "n = " << n << endl;
        for (int i = 0; i < n; ++i){
            arr[i].print();
            arr[i].calculate().print();
        }
    }
    catch(RationalException exc) {
        cout << "RationalException " << exc.getMessage() << endl;
    }
    return 0;
}
