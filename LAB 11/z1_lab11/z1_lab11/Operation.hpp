//
//  Operation.hpp
//  z1_lab11
//
//  Created by Maria Poklonskaya on 16.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once



#include <iostream>

using namespace std;

#include "Rational.hpp"
#include "RationalException.hpp"


class Operation {
private:
    char signOfOper;
    Rational operand1, operand2;
    Rational strToRational(char* str);
public:
    Operation(char s = '+', Rational o1 = Rational(), Rational o2 = Rational())
        :signOfOper(s), operand1(o1), operand2(o2){}
    Operation (char* buffer);
    void print() {
        cout << "(\t" << signOfOper << "\n\t";
        operand1.print();
        cout << '\t';
        operand2.print();
        cout << " )" << endl;
    }
    Rational calculate(){
        switch(this->signOfOper){
            case '+' :
                return operand1 + operand2;
        }
        return Rational();
    }
};

