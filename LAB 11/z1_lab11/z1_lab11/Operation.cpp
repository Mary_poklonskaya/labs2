//
//  Operation.cpp
//  z1_lab11
//
//  Created by Maria Poklonskaya on 16.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#include "Operation.hpp"

Operation::Operation (char* buffer) {
    char* p;
    char* ptr[3];
    int count = 0;
    p = strtok(buffer, " ");
    while (p != NULL){
       // cout << p << endl;
        ++count;
        if (count <= 3){
            ptr[count - 1] = p;
        }
        p = strtok (NULL, " ");
    }
    if (count != 3){
        throw RationalException ("Конструктор Operation: нельзя создать объект");
    }
    this->operand1 = strToRational(ptr[0]);
    this->signOfOper = ptr[1][0];
    this->operand2 = strToRational(ptr[2]);
}

Rational Operation::strToRational(char* str){
    char* p = strchr(str, '/');
    if (p != NULL){
        char str1[10], str2[10];
        strncpy(str1, str, p - str);
        strcpy(str2, p + 1);
        return Rational(atoi(str1), atoi(str2));
    } else {
        return Rational (atoi(str));
    }
}

