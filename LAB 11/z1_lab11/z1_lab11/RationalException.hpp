//
//  RationalException.hpp
//  z1_lab11
//
//  Created by Maria Poklonskaya on 16.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once
#include <cstring>

class RationalException{
private:
    char message[100];
public:
    RationalException (char* message){
        strcpy(this->message, message);
    }
    char* getMessage(){
        return message;
    }
};
