/* Разработать шаблон класса «Многочлен» – Polynom степени n . Написать несколько конструкторов, в том числе конструктор копирования. Реализовать методы для вычисления значения полинома; сложения, вычитания. Перегрузить операции сложения, вычитания, индексирования, присваивания. */


#pragma once

#include <iostream>


using namespace std;

template <class T>
class Polynomial{
private:
    T* value;
    int size;
public:
    Polynomial(){
        size = 10;
        for (int i = 0; i < size; ++i){
            value[i] = 0;
        }
    }
    Polynomial(int size, T value = 0): size(size){
        for (int i = 0; i < size; ++i){
            this->value[i] = value;
        }
    }
    
    Polynomial(const Polynomial& p2){
        this->size = p2.size;
        this->value = new T(this->size);
        
        for(int i = 0; i < this->size; ++i){
            this->value[i] = p2.value[i];
        }
    }
    
    int& operator[](int index) {
        return value[index];
    }
    
    T valueOfPolynom (T x){
        T res;
        for(int i = 0; i < size; ++i){
            res += value[i] * x^i;
        }
    }
    
    Polynomial operator+ (Polynomial& p2){
        Polynomial sum(max(size, p2.size));
        if (size <= p2.size){
            for(int i = size + 1; i < p2.size; ++i){
                value[i] = 0;
            }
        } else {
            for(int i = p2.size + 1; i < size; ++i){
                p2[i] = 0;
            }
        }
        for(int i = 0; i <= sum.size; ++i){
            sum[i] = value[i] + p2[i];
        }
        return sum;
    }
    
    Polynomial operator- (Polynomial& p2){
        Polynomial sum(max(size, p2.size));
        if (size < p2.size){
            for(int i = size + 1; i < p2.size; ++i){
                value[i] = 0;
            }
        } else {
            for(int i = p2.size + 1; i < size; ++i){
                p2[i] = 0;
            }
        }
        for(int i = 0; i <= sum.size; ++i){
            sum[i] = value[i] - p2[i];
        }
        return sum;
    }
    
    Polynomial& operator= (Polynomial& p2){
        if (this == &p2){
            return *this;
        }
    
        this->size = p2.size;
        this->value = new T[this->size];
        for(int i = 0; i < this->size; ++i){
            this->value[i] = p2.value[i];
        }
        return *this;
    }
    
    string toString(){
        string p2;
        for (int i = 0; i < size; ++i){
            p2 += value[i] + " * x^" + i;
            p2 += " ";
        }
        return p2;
    }
    
    int getSize(){
        return size;
    }
};
