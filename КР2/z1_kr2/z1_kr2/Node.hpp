//  Created by Maria Poklonskaya on 19.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
#pragma once

template <class T>

class Node {
public:
    T value;
    Node<T>* next;
    Node(const T& value1, Node<T>* next1):value(value1),next(next1){};
    void print(){
        cout << value.toString() << endl;
    }
};



