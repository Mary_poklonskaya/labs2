// 1.  Разработать шаблон класса Queue.


#pragma once
#include <iostream>
#include "Node.hpp"

using namespace std;

template<class T>
class Queue {
    Node<T>* head;
    Node<T>* tail;
public:
    Queue(){
        head = 0;
        tail = 0;
    }
    void push(const T& z){
        Node<T>* newNode = new Node<T>(z, 0);
        if (head == 0){
            head = newNode;
        } else {
            tail->next = newNode;
        }
        tail = newNode;
    }
    T& top() {
        return head->value;
    }
    void pop(){
        if (head != 0){
            Node<T>* temp = head->next;
            delete head;
            head = temp;
        }
    }
    bool isEmpty() {
        return(head == 0);
    }
    void print(){
        cout << "Queue: " << endl;
        for (Node<T>* node = head; node != NULL; node = node->next){
            node->print();
        }
    }
};



