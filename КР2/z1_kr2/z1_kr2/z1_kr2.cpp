// Контрольная работа 2. Вариант 2
// Поклонская Мария
//3. Прочитать коэффициенты нескольких полиномов из файла. Создать очередь объектов класса. Передать его в функцию, вычисляющую сумму полиномов степени, кратной 3 и возвращающую полином-результат, который выводится на экран в головной программе.

#include <iostream>
#include <fstream>
#include <cstring>
#include "Polynom.hpp"
#include "Queue.hpp"

using namespace std;

Polynomial<int> strToPolynom(char* str){
    char* p = strtok(str, " ");
    if (p != NULL){
       Polynomial<int> pol1;
        while (p != NULL){
            int i = 0;
            pol1[i] = atoi(p);
            ++i;
            p = strtok(NULL, " ");
        }
        return pol1;
    } else {
        return 0;
    }
}

int main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    char buffer[100];
    Queue<Polynomial<int>> queueOfPolynom();
    if (fin) {
        while (!fin.eof()) {
            fin.getline(buffer, 100);
            queueOfPolynom().push(strToPolynom(buffer));
        }
        queueOfPolynom().print();
    }
    else {
        cout << "File not open\n";
    }
    Polynomial<int> sum;
    while(!queueOfPolynom().isEmpty()){
        Polynomial<int> temp;
        temp = queueOfPolynom().top();
        if (temp.getSize() % 3 == 0){
            sum = sum + temp;
        }
        queueOfPolynom().pop();
    }
    cout << sum.toString() << '\n';
    fin.close();
    fout.close();
    
    return 0;
}
