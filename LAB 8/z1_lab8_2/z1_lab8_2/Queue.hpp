//
//  Queue.hpp
//  z1_lab8_2
//
//  Created by Maria Poklonskaya on 10.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#pragma once
#include <iostream>
#include "Node.hpp"

using namespace std;

class Queue {
    Node* head;
    Node* tail;
public:
    Queue(){
        head = 0;
        tail = 0;
    }
    void push(Complex z){
        Node* newNode = new Node(z, 0);
        if (head == 0){
            head = newNode;
        } else {
            tail->next = newNode;
        }
        tail = newNode;
    }
    void pop(){
        if (head != 0){
            Node* temp = head->next;
            delete head;
            head = temp;
        }
    }
    bool isEmpty() {
        return(head == 0);
    }
    void print(){
        cout << "Queue: " << endl;
        for (Node* node = head; node != NULL; node = node->next){
            node->print();
        }
    }
};
