//
//  complex.cpp
//  z1_lab8_2
//
//  Created by Maria Poklonskaya on 09.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

#include <iostream>
#include <cmath>
#include "complex.hpp"

using namespace std;

double Complex::lengthV(){
    return sqrt(re * re + im * im);
}

Complex::Complex(double re, double im){
    this->re = re;
    this->im = im;
}

Complex::Complex(double re){
    this->re = re;
    im = 0.;
}

Complex::Complex(){
    re = 0.;
    im = 0.;
}

Complex::Complex(const Complex& z2){
    re = z2.re;
    im = z2.im;
}

Complex Complex::operator+ (Complex z2){
    return Complex(re + z2.re, im + z2.im);
}

Complex Complex::operator- (Complex z2){
    return Complex(re - z2.re, im - z2.im);
}

Complex Complex::operator* (Complex z2){
    return Complex(re * z2.re - im * z2.im, re * z2.im + im * z2.re);
}

Complex Complex::operator/ (Complex z2){
    double a = re;
    double b = im;
    double c = z2.re;
    double d = z2.im;
    return Complex((a*c + b * d) / (c*c + d * d), (b*c - a * d) / (c*c + d * d));
}

Complex& Complex::operator= (const Complex& z2){
    re = z2.re;
    im = z2.im;
    return *this;
}

bool Complex::operator== (Complex z2){
    return (re == z2.re && im == z2.im);
}

bool Complex::operator< (Complex z2){
    return (this->lengthV() < z2.lengthV());
}

bool Complex::operator> (Complex z2){
    return (this->lengthV() > z2.lengthV());
}

string Complex::toString(){
    string complexNum = to_string(re);
    complexNum += " + i * ";
    complexNum += to_string(im);
    return complexNum;
   // cout << re << " + i * " << im << endl;
}

bool operator!= (Complex z1, Complex z2){
    return !(z1 == z2);
}

