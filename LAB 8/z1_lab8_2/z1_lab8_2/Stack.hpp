//
//  Stack.hpp
//  z1_lab8_2
//
//  Created by Maria Poklonskaya on 09.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//

/*  Разработать классы «стек» и «элемент» для работы с объектами «комплексное число». Реализовать операции добавления и удаления элементов в/из структуры данных.*/

#pragma once
#include <iostream>
#include "Node.hpp"

using namespace std;

class Stack {
    Node* head;
public:
    Stack():head(NULL){};
    void push(Complex z) {
        Node* newNode = new Node(z, head);
        head = newNode;
    }
    void pop(){
        if (head != 0){
            Node* temp = head->next;
            delete head;
            head = temp;
        }
    }
    
    bool isEmpty() {
        return (head == 0);
    }
    
    void print(){
        cout << "Stack: " << endl;
        for (Node* node = head; node != NULL; node = node->next){
            node->print();
        }
    }
};
