//
//  Node.hpp
//  z1_lab8_2
//
//  Created by Maria Poklonskaya on 09.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
#pragma once
#include "complex.hpp"

class Node {
public:
    Complex value;
    Node* next;
    Node(Complex value1, Node* next1):value(value1),next(next1){};
    void print(){
        cout << value.toString() << endl;
    }
};
