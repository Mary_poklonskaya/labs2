//
//  complex.hpp
//  z1_lab8
//
//  Created by Maria Poklonskaya on 09.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//
/* 1.    Разработать класс «комплексное число». Класс должен содержать:
 •    конструкторы, в том числе конструктор по умолчанию и конструктор копирования;
 •    методы сложения, вычитания, умножения, деления, присваивания, сравнения для комплексных чисел (например по критерию «длина вектора»);
 */

#pragma once
#include <iostream>
#include <cstring>

using namespace std;

class Complex {
private:
    double re, im;
    double lengthV();
public:
    Complex(double, double);
    Complex(double);
    Complex();
    Complex(const Complex&);
    string toString();
    Complex operator+ (Complex z2);
    Complex operator- (Complex z2);
    Complex operator* (Complex z2);
    Complex operator/ (Complex z2);
    Complex& operator= (const Complex&);
    bool operator== (Complex z2);
    bool operator< (Complex z2);
    bool operator> (Complex z2);
};

bool operator!= (Complex z1, Complex z2);
