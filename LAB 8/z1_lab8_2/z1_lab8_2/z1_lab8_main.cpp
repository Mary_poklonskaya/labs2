//
//  main.cpp
//  z1_lab8_2
//
//  Created by Maria Poklonskaya on 09.12.2018.
//  Copyright © 2018 Maria Poklonskaya. All rights reserved.
//


#include <iostream>
#include "complex.hpp"
#include "Stack.hpp"
#include "Queue.hpp"

using namespace std;

Complex findMax(Complex arr[], int n){
    Complex max = arr[0];
    for (int i = 1; i < n; ++i){
        if (max < arr[i]){
            max = arr[i];
        }
    }
    return max;
}

int main() {
    Complex c1(2, 5.1), c2, c3(3.5), c4(c3);
    cout << c1.toString() << endl;
    cout << c2.toString() << endl;
    cout << c3.toString() << endl;
    cout << c4.toString() << endl;
    
    //Complex sum = c1 + c3;
    Complex sum = c1.operator+(c3);
    sum.toString();
    Complex diff;
    diff = c1 + c2;
    cout << diff.toString() << endl;

    cout << "с2 < c1 = " << (c2 < c1) << endl;
    cout << "с1 == c2 = " << (c1 == c2) << endl;
    cout << "с1 != c2 = " << (c1 != c2) << endl;
//Протестировать работу на массиве комплексных чисел, найдя наибольшее/наименьшее по длине вектора комплексное число
    const int N = 5;
    Complex arr[N] = {Complex(2, 3.2), Complex(4, 3.2), Complex(2, 0), Complex(-2, 3.2), Complex(0, 2)};
    Complex max = findMax (arr, N);
    max.toString(); 
    
    Stack st;
    st.push(c1);
    st.print();
    st.push(c2);
    st.print();
    st.pop();
    st.pop();
    st.pop();
    
    Queue qu;
    qu.print();
    qu.push(c2);
    qu.print();
    qu.push(c3);
    qu.push(c1);
    qu.print();
    qu.pop();
    qu.print();

    
    return 0;
}
