//Lab 1. Ex 1.
//  Created by Maria Poklonskaya on 04.09.2018.

/*  Известны площадь квадрата и площадь круга. Определить:
 a)    уместится ли квадрат в круге;
 b)    уместится ли круг в квадрате.  */

/* Test casing:
 1. Входные дынные:  Enter the square (S) of the circle: // радиус круга = половине стороны квадрата;
                     78.5
                     Enter the square (S) of the square:
                     100
    Выходные дынные: A square can't be placed in a circle;
                     A circle can be placed in a square;
 2. Входные дынные:  Enter the square (S) of the circle: // радиус круга = половине диагонали = 5;
                     78.5
                     Enter the square (S) of the square:
                     50.29
    Выходные дынные: A square can be placed in a circle;
                     A circle can't be placed in a square;
 3. Входные дынные:  Enter the square (S) of the circle: // радиус круга > половине диагонали
                    78.5
                     Enter the square (S) of the square:
                     50.3
    Выходные дынные: A square can't be placed in a circle;
                     A circle can't be placed in a square;
 4. Входные дынные:  Enter the square (S) of the circle: // радиус круга < половине стороны квадрата;
                     154
                     Enter the square (S) of the square:
                     256
    Выходные дынные: A square can't be placed in a circle;
                     A circle can be placed in a square;
 5. Входные дынные:  Enter the square (S) of the circle:
                     -4
                     Enter the square (S) of the square:
                     35
    Выходные дынные: Possible only for positive squares
 6. Входные дынные:  Enter the square (S) of the circle:
                     0
                     Enter the square (S) of the square:
                     0
    Выходные дынные: A square can be placed in a circle;
                     A circle can be placed in a square;
 */
#include <iostream>
#include <math.h>

using namespace std;

int main() {
    double s_kr;   // площадь круга;
    double s_kv;   // площадь квадрата;
    double d;      // половина диагонали квадрата;
    double r;      // радиус круга;
    double a;      // половина стороны квадрата;
    const double PI = 3.14;
        cout << "Enter the square (S) of the circle: " << endl;
        cin >> s_kr;
        cout << "Enter the square (S) of the square: " << endl;
        cin >> s_kv;
    if (s_kr >= 0 && s_kv >= 0) {
        d = sqrt(s_kv/2);
        r = sqrt(s_kr/PI);
        a = sqrt(s_kv)/2;
        if (d <= r) {
            cout << "A square can be placed in a circle;"<<endl;
        } else {
            cout << "A square can't be placed in a circle;"<<endl;
        }
        if (r <= a) {
            cout << "A circle can be placed in a square;"<<endl;
        } else {
            cout << "A circle can't be placed in a square;"<<endl;
        }
    } else {
        cout << "Possible only for positive squares " << endl;
    }
    
    return 0;
}
